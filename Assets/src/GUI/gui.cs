﻿using UnityEditor;
using UnityEngine;
using System.Collections.Generic;
namespace utility
{
    static public class GUI
    {
        #region button
        static public bool test
        {
            get
            {
                return (GUILayout.Button("ТЕСТ"));
            }

        }
        static public bool кнопка(string name = "button",byte width=80)
        {
            return (GUILayout.Button(name, new GUILayoutOption[] { GUILayout.Width(width) }));
        }
        static public bool кнопка(ref bool B, string name = "button")
        {
            bool b = (GUILayout.Button(name));
            if (b)
                B = !B;
            return b;
        }
        #endregion
        #region bool
        static public bool логический(this ref bool b, string name = "bool", byte width = 80)
        {
            bool new_b = EditorGUILayout.Toggle(name, b, new GUILayoutOption[] { GUILayout.Width(width) });
            if (new_b != b)
            {
                b = new_b;
                return true;
            }
            return false;
        }
        static public bool логический(ref List<bool> bs, byte cnt)
        {
            EditorGUILayout.BeginVertical();
            bool B = false;
            bool b = false;

            for (byte i = 0; i < cnt; i++)
            {
                if (bs.Count != cnt) bs.Add(false);
                if (!B)
                {
                    b = bs[i];
                    if (b.логический(i.ToString()))
                    {

                        bs[i] = b;
                        B = true;
                        break;
                    }
                }
            }
            EditorGUILayout.EndVertical();
            return B;
        }
        #endregion

        #region ползунок
        static public bool ползунок(this ref sbyte lv, int min = 1, int max = 5)
        {
            sbyte new_lv = (sbyte)(EditorGUILayout.IntSlider(lv, min, max));
            if (new_lv != lv)
            {
                lv = new_lv;
                return true;
            }
            return false;
        }
        static public bool ползунок(this ref int lv, int min = 1, int max = 5)
        {
            int new_lv = EditorGUILayout.IntSlider(lv, min, max);
            if (new_lv != lv)
            {
                lv = new_lv;
                return true;
            }
            return false;
        }
        static public bool ползунок(this ref Vector3 v, int min = 0, int max = 8)
        {
            int x = EditorGUILayout.IntSlider((int)v.x, min, max);
            int y = EditorGUILayout.IntSlider((int)v.y, min, max);
            int z = EditorGUILayout.IntSlider((int)v.z, min, max);
            if (x != v.x ||
                y != v.y ||
                z != v.z)
            {
                v = new Vector3(x, y, z);
                return true;
            }
            return false;
        }
        static public bool ползунок(this ref byte lv, int min = 1, int max = 5)
        {
            byte new_lv = (byte)(EditorGUILayout.IntSlider(lv, min, max));
            if (new_lv != lv)
            {
                lv = new_lv;
                return true;
            }
            return false;
        }
        #endregion

        #region вектор
        static public bool Вектор(this ref Vector2 v, string name = "v")
        {
            Vector2 new_v = EditorGUILayout.Vector2Field(name, v);
            if (new_v != v)
            {
                v = new_v;
                return true;
            }
            return false;
        }
        static public bool Вектор(this ref Vector3 v, string name = "v")
        {
            Vector3 new_v = EditorGUILayout.Vector3Field(name, v);
            if (new_v != v)
            {
                v = new_v;
                return true;
            }
            return false;
        }
        #endregion

        #region color
        static public bool Цвет(this ref Color с)
        {
            Color new_color = EditorGUILayout.ColorField(с);
            if (new_color != с)
            {
                с = new_color;
                return true;
            }
            return false;
        }
        #endregion

        static public bool SelectionGrid(this ref byte val_byte, string[] ops)
        {
            byte new_byte = (byte)(GUILayout.SelectionGrid(val_byte, ops, 6, "toggle"));
            if (new_byte != val_byte)
            {
                val_byte = new_byte;
                return true;
            }
            return false;
        }


        static public void Foldout(System.Func<bool>[]выполнить,string[] Name, bool[] showPosition)
        {
            List<string> f = new List<string>();
            for (byte i = 0; i < showPosition.Length; i++)
            {
                showPosition[i] = EditorGUILayout.Foldout(showPosition[i], Name[i]);
                if (showPosition[i])
                    if (Selection.activeTransform)
                        выполнить[i]();                     
                if (!Selection.activeTransform)
                    showPosition[i] = false;
            }
        }
    }
}
