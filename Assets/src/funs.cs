﻿//СВОРАЧИВАТЬ/РАЗВОРАЧИВАТЬ ВЫДЕЛЕННЫЙ РЕГИОН CTR+M
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using utility.combine;
using utility.go;

static public class funs
{
    #region Числа
    static public string Уникальный
        {
            get
            {
                string s = System.DateTime.Now.ToString();
                s = s.Replace(":", "-");
                s = s.Replace("/", "");
                s = s.Replace("PM", "");
                s = s.Replace("AM", "");
                return s + Random.Range(0, 1000);
            }
        }
    static public ulong УникальныйИД
        {
            get
            {
                return System.Convert.ToUInt64(Уникальный);
            }
        }
    #endregion

    #region РеброЧанка
    static public Vector3 ВершинаРебра(this Vector3 v0, ushort i, byte n, float r)
    {
        return v0.Ребро(i.ребро(n), r);
    }
    static public Vector3 Ребро(this Vector3 v0, ushort edge, float r)
    {
        var dv = math_cell_triangle.Вершины[math_cell_triangle.РеброПоВершине[edge, 0]];
        var v = v0 + r * dv;
        var id = oop.Chunk.Координата(v);
        var vx = VertexGlobal.CELLS[id];
        var i = math_cell_triangle.РеброПоВершине[edge, 1];

        vx.ДобавитьВершину(i);

        return vx.Рёбра[i] + vx.dv[i];
    }
    static void ДобавитьВершину(this VertexGlobal vx, byte i)
    {
        vx.Использует[i] = true;
        if (VertexGlobal.temp.FindIndex(x => x == vx) == -1)
            VertexGlobal.temp.Add(vx);
    }
    #endregion

    #region СОБРАТЬ ЧАНК
    static List<ulong> ПостроеныВершиныЧанка = new List<ulong>();
    static Dictionary<uint, List<Mesh>> tms = new Dictionary<uint, List<Mesh>>();
    static public void СобратьЯчейкиЧанка(this Root root)
    {
        //var r = root.R;
        var r = 0;
        for (var z = -1; z <= +1; z++)
            for (var y = -1; y <= +1; y++)
                for (var x = -1; x <= +1; x++)
                {
                    //root.СобратьЯчейкуЧанка(x, y, z, r);
                }
    }
    static bool УжеДобавленаЯчейка(this Vector3 v0)
    {
        ulong ID = oop.Chunk.Координата(v0);
        if (ПостроеныВершиныЧанка.FindIndex(t => t == ID) != -1) return true;
        ПостроеныВершиныЧанка.Add(ID);
        return false;
    }
    #endregion
}
