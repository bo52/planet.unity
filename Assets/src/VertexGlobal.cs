﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using utility.go;

public class VertexGlobal
{
    static public Dictionary<ulong, VertexGlobal> CELLS = new Dictionary<ulong, VertexGlobal>();
    static public List<VertexGlobal> temp = new List<VertexGlobal>();

    #region Ребро
    static public Vector3 РеброЦентра(Vector3 центр, ushort edge,float r)
    {
        return РеброВершины(центр + r * math_cell_triangle.Вершины[math_cell_triangle.РеброПоВершине[edge, 0]], edge, r);
    }
    static public Vector3 РеброВершины(Vector3 вершина, ushort edge, float r)
    {
        return вершина + r * math_cell_triangle.ДвижениеПоРебру[edge];
    }
    public Vector3 Ребро(byte ось)
    {
        return глВектор + oop.Chunk_funs.get_R(scale) *math_cell_triangle.ТриОси[ось];
    }
    #endregion

    #region Подсветить Вершины
    #endregion

    ulong ИД;
    public Vector3 глВектор;
    public bool[] Использует = new bool[3];
    public Vector3[] Рёбра = new Vector3[3];
    public Vector3[] dv = new Vector3[3];
    public bool активный = false;
    public sbyte scale = 0;

    public VertexGlobal(ulong ИД, Vector3 Вершина, sbyte scale)
    {
        this.scale = scale;
        var r = 0;
        //var r = Root.get_R(scale);
        глВектор = Вершина;
        for (var i = 0; i < 3; i++)
        {
            Рёбра[i] = глВектор + r * math_cell_triangle.ТриОси[i];
        }
        if (активный) return;
    }
}
