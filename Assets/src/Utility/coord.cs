﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace utility
{
    static public class coord
    {
        static public uint Координата(this bool[] bs, byte Max)
        {
            uint id = 0;
            uint max = 0;
            for (byte i = 0; i < Max; i++)
            {
                max += (uint)Mathf.Pow(2, i);
                id += bs[i] ? max : 0;
            }
            return id;
        }
        static public bool[] Координата(this float id, byte Max)
        {
            float max = 0;
            bool[] bs = new bool[Max];
            for (byte i = 0; i < Max; i++)
                max += (uint)Mathf.Pow(2, i);

            int n = Max-1;
            while (n > -1) {
                bs[n]=id / max >= 1;
                id -= max;
                max-= Mathf.Pow(2, n);
                n--;
            }

            return bs;
        }

        static public uint Координата(this byte[] bs, byte Max,byte ВАРИАЦИЯ=2)
        {
            float id = 0;
            float max = 0;
            bool[] BS = new bool[Max];
            foreach (byte f in bs)
                BS[f] = true;

            for (byte f=0;f<Max;f++)
            {
                max += Mathf.Pow(ВАРИАЦИЯ, f);
                id += BS[f]? max : 0;
            }
            return (uint)id;
        }
    }
}
