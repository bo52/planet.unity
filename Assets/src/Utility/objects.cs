﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System.IO;
using UnityEditor;

namespace utility.go
{
    static public class objects
    {
        static public void МодификацияВершин(this GameObject p,Vector3 old_v, Vector3 new_v)
        {
            Mesh oMesh;
            foreach (Transform child in p.transform)
            {
                oMesh = child.gameObject.GetComponent<MeshFilter>().sharedMesh;
                Vector3[] vertices = oMesh.vertices;
                for (var n = 0; n < oMesh.vertices.Length; n++)
                {
                    if (oMesh.vertices[n] == old_v)
                    {
                        vertices.SetValue(new_v, n);
                    }
                }
                oMesh.vertices = vertices;
                oMesh.RecalculateNormals();
            }

        }
        /// <summary>
        /// один из встроенных шейдеров
        /// </summary>
        static string шейдер = "Custom/Shader5_2d";

        static public void УдалениеОбъектов(this string w_name)
        {
            УдалениеОбъектов(simple.manager.world_name(w_name).transform);
        }
        static public void УдалениеОбъектов(this Transform p)
        {
            while (p.childCount != 0)
                foreach (Transform child in p)
                {
                    Object.DestroyImmediate(child.gameObject);
                }
        }
        static public GameObject СозданиеОбъекта(this Transform p)
        {
            GameObject go = new GameObject();
            go.transform.parent = p;
            go.name = "block";
            return go;
        }
        static public GameObject СозданиеОбъекта(this Transform p, Vector3 v, sbyte scale)
        {
            GameObject go = p.СозданиеОбъекта();
            go.name = scale + ":" + v;
            go.transform.localPosition = v;
            return go;
        }

        static public readonly string[] mat_DIRS=new string[]{
            "materials/ID/Materials/",
            "materials/8192/",
            "materials/textures/",
        };
        static public void СозданиеМатериала(this GameObject go,uint mat = 0, byte mat_dir = 0)
        {
            MeshRenderer mr = go.GetComponent<MeshRenderer>();
            if (mr == null)
            {

                mr = go.AddComponent<MeshRenderer>();
                //mr.material = new Material(Shader.Find(шейдер));             
            }
            mr.sharedMaterial = Resources.Load(mat_DIRS[mat_dir] + mat + "_COLOR", typeof(Material)) as Material;
            //mr.sharedMaterial.mainTexture = Resources.Load("materials/ID/"+mat+"_COLOR", typeof(Texture2D)) as Texture2D;
        }

        static public void ПривязатьМешКОбъекту(this Mesh M, GameObject go)
        {
            if (M == null) return;
            go.СозданиеМатериала();

            MeshFilter mf=go.GetComponent<MeshFilter>();
            if (mf==null)
                go.AddComponent<MeshFilter>().sharedMesh = M;
            else
                mf.sharedMesh = M;

            MeshCollider mc= go.GetComponent<MeshCollider>();
            if (mc == null)
                go.AddComponent<MeshCollider>().sharedMesh = M;
            //else
                //mc.sharedMesh = M;
        }

        static public GameObject ПривязатьМешКОбъекту(this Mesh M, Transform p,byte Masklayer=0, uint mat = 0,byte mat_dir=0)
        {
            if (M == null) return null;
            GameObject go = p.СозданиеОбъекта();
            go.transform.localPosition = Vector3.zero;
            go.СозданиеМатериала(mat, mat_dir);
            if (Masklayer!=0)
                go.layer = Masklayer;

            go.AddComponent<MeshFilter>().sharedMesh = M;
            go.AddComponent<MeshCollider>().sharedMesh = M;

            return go;
        }
        static public GameObject ПривязатьМешКОбъектуОчищенно(this Mesh M, Transform p, byte Masklayer = 0, uint mat = 0, byte mat_dir = 0)
        {
            p.УдалениеОбъектов();
            return M.ПривязатьМешКОбъекту(p, Masklayer, mat, mat_dir);
        }

        static public GameObject ПривязатьМешКОбъекту(this меш m, Transform p)
        {
            Mesh M = m.ToMesh();
            if (M == null) return null;
            return M.ПривязатьМешКОбъекту(p);
        }

        static public GameObject ЗагрузитьПрефаб(this string file, Vector3 v,Transform p)
        {
            GameObject go = Object.Instantiate(Resources.Load(file, typeof(GameObject))) as GameObject;
            go.transform.position = Vector3.zero;
            go.transform.SetParent(p);
            go.transform.localPosition = v;
            return go;
        }
        #region WORLD
        static public task.Region.world w_REGION(this GameObject go)
        {
            return go.GetComponent<task.Region.world>();
        }
        #endregion
    }
}