﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using utility.go;
namespace utility
{
    public class меш
    {
        ///восемь вершин кубика (координаты)
        static readonly public Vector3[] ВершиныКуба = new Vector3[8] {
            new Vector3(-1, -1, -1),
            new Vector3(+1, -1, -1),
            new Vector3(-1, +1, -1),
            new Vector3(+1, +1, -1),
            new Vector3(-1, -1, +1),
            new Vector3(+1, -1, +1),
            new Vector3(-1, +1, +1),
            new Vector3(+1, +1, +1)
        };
        static readonly public byte[,] ВершиныГранейКуба = new byte[6, 4] {
            { 2, 6, 0, 4 },
            { 7, 3, 5, 1 },
            { 4, 5, 0, 1 },
            { 2, 3, 6, 7 },
            { 3, 2, 1, 0 },
            { 6, 7, 4, 5 }
        };
        /// vertex - вершины
        /// </summary>
        public List<Vector3> vs = new List<Vector3>();
        /// <summary>
        /// triangles - индексы вершин для квадратов (двух треугольников)
        /// </summary>
        public List<int> ts = new List<int>();
        /// <summary>
        /// развёртка для квадратов (двух треугольников)
        /// </summary>
        public List<Vector2> uvs = new List<Vector2>();
        public virtual void очистить()
        {
            vs.Clear();
            ts.Clear();
            uvs.Clear();
        }

        #region ПривязатьМешКОбъекту

        public virtual void Собрать() { }
        public virtual void Собрать(Dictionary<ushort, uint> Content) { }
        public void Выполнить(simple.view ТЕСТ)
        {
            очистить();
            if (ТЕСТ!=null)
                ТЕСТ.Выполнить();
            Собрать();
        }
        /// <summary>
        /// p - Игровой Объект родитель
        /// </summary>
        /// <param name="p"></param>
        /// <param name="ТЕСТ"></param>
        public GameObject ПривязатьМешКОбъекту(Transform p, simple.view ТЕСТ=null)
        {
            p.УдалениеОбъектов();
            Выполнить(ТЕСТ);
            return objects.ПривязатьМешКОбъекту(this,p);
        }
        /// <summary>
        /// собираем результат меша
        /// </summary>
        public Mesh ToMesh()
        {
            if (vs.Count == 0)
                return null;

            Mesh mesh = new Mesh();
            mesh.vertices = vs.ToArray();
            mesh.triangles = ts.ToArray();

            //развёртка
            mesh.uv = uvs.ToArray();
            mesh.RecalculateNormals();
            mesh.RecalculateBounds();

            return mesh;
        }
        #endregion
    }
}
