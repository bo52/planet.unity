﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace utility
{
    static public class math_quad
    {
        static public Vector2 Вёрстка(this byte F, Vector3 v)
        {
            switch (F)
            {
                case 0://left
                case 1://right
                    return new Vector2(v.z, v.y);
                case 2://down
                case 3://up
                    return new Vector2(v.x, v.z);
                case 4://back
                case 5://front
                    return new Vector2(v.x, v.y);
            }
            return Vector2.zero;
        }

        static public Vector3 Вершина(this byte F, Vector3 Центр, byte i_quad, sbyte scale = 0)
        {
            return Центр + Mathf.Pow(2, scale - 1) * меш.ВершиныКуба[simple.меш.ВершиныГранейКуба[F, i_quad]];
        }

        static public Mesh quad(this byte F, Vector3 Центр, sbyte scale = 0)
        {
            var vs = new[] { F.Вершина(Центр, 0, scale), F.Вершина(Центр, 1, scale), F.Вершина(Центр, 2, scale), F.Вершина(Центр, 3, scale) };

            var mesh = new Mesh
            {
                vertices = vs,
                uv = new[] { F.Вёрстка(vs[0]), F.Вёрстка(vs[1]), F.Вёрстка(vs[2]), F.Вёрстка(vs[3]) },
                triangles = new[] { 0, 2, 3, 3, 1, 0 },
            };
            return mesh;
        }
    }
}
