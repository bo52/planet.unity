﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
namespace utility
{
    static public class math_cubes 
    {
        static string FILE = "/CS.ncs";
        //
        static public Dictionary<ulong, uint[,]> CS = new Dictionary<ulong, uint[,]> {
            { 0,null},
            { 1,new uint[,]
            {
                {120 },{120 },{0, },{120, },{0, },{0, },{0, },{0, }
            } 
            },
        };
        //static public Dictionary<ulong, ulong> CS = new Dictionary<ulong, List<uint[]>>();
        //0 - номер идентификатора Dictionary
        //0 - номер куба (0-7)
        //1 - fs код поверхностей 120
        //2 - T ссылка на текстуру
        //3 - dv код offset
        //4 - MS ссылка на модель
        #region СОХРАНИТЬ/ЗАГРУЗИТЬ
        //Сохранение
        static public string Сохранить(uint[,]cs)
        {
            string s = "";
            for (byte i = 0; i < 8; i++)
            {
                if (i != 0)
                    s += "=";
                for (byte j = 0; j < 4; j++)
                {
                    if (j != 0) s += ":";
                    s += cs[i, j];
                    
                }
            }
            return s;
        }
        static public void Сохранить()
        {
            StreamWriter sw = new StreamWriter(Application.dataPath + FILE, false);
            foreach (KeyValuePair<ulong, uint[,]> cs in CS)
            {
                sw.WriteLine(cs.Key+" "+ Сохранить(cs.Value));
            }
            sw.Close();
        }

        static public uint[] cs(this string s)
        {
            string[] rows = s.Split(':');
            uint[] cs = new uint[rows.Length];
            int cnt = 0;
            foreach (var row in rows)
            {
                cs[cnt] = System.Convert.ToUInt32(row);
                cnt++;
            }
            return cs;
        }

        static public uint[,] ms(this string s)
        {
            uint[,] ms = new uint[8,4];
            int i = 0;
            foreach (var row in s.Split('='))
            {
                int j = 0;
                foreach (var cs in row.Split(':'))
                {
                    ms[i,j]= System.Convert.ToUInt32(cs);
                }
                i++;
            }
            return ms;
        }
        static public void Загрузить()
        {
            CS.Clear();
            string[] rows = File.ReadAllLines(Application.dataPath + FILE);
            int n = -1;
            for (int i = 0; i < rows.Length; i++)
            {
                n = rows[i].IndexOf(' ');
                if (n == -1) continue;
                CS.Add(System.Convert.ToUInt64(
                    rows[i].Substring(0,n-1)), 
                    rows[i].Substring(n+1).ms()
                    );
            }
        }
        #endregion

    }
}
