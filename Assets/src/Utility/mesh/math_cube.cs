﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace utility
{
    static public class math_cube
    {

        static public Mesh cube(this Vector3 Центр,byte[] F = null, int scale=0)
        {
            var combine = new CombineInstance[6];
            foreach (byte f in F)
                combine[f].mesh = f.quad(Центр,(sbyte)scale);

            var mesh = new Mesh();
            mesh.CombineMeshes(combine, true, false);
            return mesh;
        }
        static public Mesh cube(this Vector3 Центр, uint fs, int scale = 0)
        {
            if (fs == 0) return null;
            return Центр.cube(fs.SQA(), scale);
        }


        static public sbyte[,] fs = new sbyte[,]
        {
                { -1 , 1,-1,2, -1 ,4 },
                { 0 ,-1,-1,3,-1, 5 },
                { -1,3, 0,-1,-1,6 },
                { 2,-1,1,-1,-1,7 },
                {-1,5,-1,6,0,-1 },
                { 4,-1,-1,7,1, -1},
                { -1, 7,4,-1,2,-1 },
                { 6,-1,5,-1,3,-1},  
        };

        static public byte СобратьПоверхности(this uint[,] cs,byte x)
        {
            List<byte> bs = new List<byte>();
            sbyte b;
            for (byte f = 0; f < 6; f++)
            {
                b = fs[x, f];
                if (b==-1)
                    bs.Add(f);
                else
                if (cs[b, 0] == 0) bs.Add(f);
            }
            return bs.МассивГранейВЧисло();
        }

        static public Mesh cube(this ulong ИД,bool Собрать=true)
        {
            ИД = 1;
            if (ИД == 0)
                return Vector3.zero.cube();
            uint[,] cs=math_cubes.CS[ИД];
            var combine = new CombineInstance[8];
            Vector3 Центр = Vector3.zero;
            int scale = 0;
            for (byte i = 0; i < cs.GetLength(0); i++)
            {
                if (cs[i, 0] == 0) continue;
                if (Собрать) cs[i, 0] = cs.СобратьПоверхности(i);
                combine[i].mesh = (Центр + 0.5f*Mathf.Pow(2, scale-1) * simple.меш.ВершиныКуба[i]).cube(cs[i,0], scale-1);
            }
            var mesh = new Mesh();
            mesh.CombineMeshes(combine, true, false);
            return mesh;
        }
    }
}
