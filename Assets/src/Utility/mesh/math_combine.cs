﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using utility.go;
namespace utility.combine
{
    static public class math_combine
    {
        static public Mesh ToMesh(this CombineInstance[] combine)
        {
            var mesh = new Mesh();
            mesh.CombineMeshes(combine, true, false);

            mesh.RecalculateBounds();
            mesh.RecalculateNormals();
            return mesh;
        }
        static GameObject ПривязатьМешКОбъекту(this CombineInstance[] combine,GameObject p)
        {
            return combine.ToMesh().ПривязатьМешКОбъекту(p.transform);
        }
    }
}
