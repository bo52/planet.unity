﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class MeshStudy : MonoBehaviour
{
    #region для Editor
    static public float radius = 0.2f;
    static public float pull = 0.3f;
    static public float handleSize = 0.005f;
    static public bool moveVertexPoint = true;
    [HideInInspector]
    static public bool isCloned = false;
    #endregion

    [HideInInspector]
    static public Vector3[] vertices;

    static Mesh oMesh;
    static Mesh cMesh;
    static MeshFilter oMeshFilter;
    static int[] triangles;

    // Start is called before the first frame update
    void Start()
    {
    }
    static public void initMesh(GameObject go)
    {
        oMeshFilter = go.GetComponent<MeshFilter>();
        oMesh = oMeshFilter.sharedMesh; //1

        cMesh = new Mesh(); //2
        cMesh.name = "clone";
        cMesh.vertices = oMesh.vertices;
        cMesh.triangles = oMesh.triangles;
        cMesh.normals = oMesh.normals;
        cMesh.uv = oMesh.uv;
        oMeshFilter.mesh = cMesh;  //3

        vertices = cMesh.vertices; //4
        triangles = cMesh.triangles;

        isCloned = true;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void DoAction(int index, Vector3 localPos)
    {
        // specify methods here
        //PullOneVertex(index, localPos);
        PullSimilarVertices(index, localPos);
    }
    
    // Pulling only one vertex pt, results in broken mesh.
    private void PullOneVertex(int index, Vector3 newPos)
    {
        vertices[index] = newPos; //1
        cMesh.vertices = vertices; //2
        cMesh.RecalculateNormals(); //3
    }
    private void PullSimilarVertices(int index, Vector3 newPos)
    {
        Vector3 targetVertexPos = vertices[index]; //1
        List<int> relatedVertices = FindRelatedVertices(targetVertexPos, false); //2
        foreach (int i in relatedVertices) //3
        {
            vertices[i] = newPos;
        }
        cMesh.vertices = vertices; //4
        cMesh.RecalculateNormals();
    }

    // returns List of int that is related to the targetPt.
    private List<int> FindRelatedVertices(Vector3 targetPt, bool findConnected)
    {
        // list of int
        List<int> relatedVertices = new List<int>();

        int idx = 0;
        Vector3 pos;

        // loop through triangle array of indices
        for (int t = 0; t < triangles.Length; t++)
        {
            // current idx return from tris
            idx = triangles[t];
            // current pos of the vertex
            pos = vertices[idx];
            // if current pos is same as targetPt
            if (pos == targetPt)
            {
                // add to list
                relatedVertices.Add(idx);
                // if find connected vertices
                if (findConnected)
                {
                    // min
                    // - prevent running out of count
                    if (t == 0)
                    {
                        relatedVertices.Add(triangles[t + 1]);
                    }
                    // max 
                    // - prevent runnign out of count
                    if (t == triangles.Length - 1)
                    {
                        relatedVertices.Add(triangles[t - 1]);
                    }
                    // between 1 ~ max-1 
                    // - add idx from triangles before t and after t 
                    if (t > 0 && t < triangles.Length - 1)
                    {
                        relatedVertices.Add(triangles[t - 1]);
                        relatedVertices.Add(triangles[t + 1]);
                    }
                }
            }
        }
        // return compiled list of int
        return relatedVertices;
    }

}
