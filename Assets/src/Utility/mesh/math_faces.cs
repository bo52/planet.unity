﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
namespace utility
{
    static public class math_faces
    {
        //var F = new byte[] { 0, 1, 2, 3, 4, 5 };
        static uint МассивВчисло(this bool[] bs)
        {
            uint id = 0;
            uint max = 0;
            for (byte i = 0; i < 6; i++)
            {
                max += (uint)Mathf.Pow(2, i);
                id += bs[i] ? max : 0;
            }
            return id;
        }

        static bool[] ПостроитьМассивГранейКуба(this List<byte> FS)
        {
            var fs = new bool[6];
            foreach (byte f in FS)
                fs[f] = true;
            return fs;
        }
        static byte МассивВЧисло(this bool[] bs)
        {
            return (byte)(bs.МассивВчисло());
        }

        static public byte МассивГранейВЧисло(this List<byte> FS)
        {
            return FS.ПостроитьМассивГранейКуба().МассивВЧисло();
        }
        static public byte[] SQA(this byte fs)
        {
            return МассивВозможностейКубика.SQA(fs);
        }
        static public byte[] SQA(this uint fs)
        {
            return МассивВозможностейКубика.SQA((byte)fs);
        }
        //шесть граней кубика перебор = один кубик
        static class МассивВозможностейКубика
        {
            static public byte[] SQA(byte fs)
            {
                return Значение[Координата.ToList().FindIndex(x=>x==fs)];
            }

            static readonly byte[] Координата = new byte[64] { 0, 1, 3, 4, 7, 8, 10, 11, 15, 16, 18, 19, 22, 23, 25, 26, 31, 32, 34, 35, 38, 39, 41, 42, 46, 47, 49, 50, 53, 54, 56, 57, 63, 64, 66, 67, 70, 71, 73, 74, 78, 79, 81, 82, 85, 86, 88, 89, 94, 95, 97, 98, 101, 102, 104, 105, 109, 110, 112, 113, 116, 117, 119, 120 };
            static readonly List<byte[]> Значение = new List<byte[]> {null,
                new byte[1]{0,},
                new byte[1]{1,},
                new byte[2]{0,1,},
                new byte[1]{2,},
                new byte[2]{0,2,},
                new byte[2]{1,2,},
                new byte[3]{0,1,2,},
                new byte[1]{3,},
                new byte[2]{0,3,},
                new byte[2]{1,3,},
                new byte[3]{0,1,3,},
                new byte[2]{2,3,},
                new byte[3]{0,2,3,},
                new byte[3]{1,2,3,},
                new byte[4]{0,1,2,3,},
                new byte[1]{4,},
                new byte[2]{0,4,},
                new byte[2]{1,4,},
                new byte[3]{0,1,4,},
                new byte[2]{2,4,},
                new byte[3]{0,2,4,},
                new byte[3]{1,2,4,},
                new byte[4]{0,1,2,4,},
                new byte[2]{3,4,},
                new byte[3]{0,3,4,},
                new byte[3]{1,3,4,},
                new byte[4]{0,1,3,4,},
                new byte[3]{2,3,4,},
                new byte[4]{0,2,3,4,},
                new byte[4]{1,2,3,4,},
                new byte[5]{0,1,2,3,4,},
                new byte[1]{5},
                new byte[2]{0,5},
                new byte[2]{1,5},
                new byte[3]{0,1,5},
                new byte[2]{2,5},
                new byte[3]{0,2,5},
                new byte[3]{1,2,5},
                new byte[4]{0,1,2,5},
                new byte[2]{3,5},
                new byte[3]{0,3,5},
                new byte[3]{1,3,5},
                new byte[4]{0,1,3,5},
                new byte[3]{2,3,5},
                new byte[4]{0,2,3,5},
                new byte[4]{1,2,3,5},
                new byte[5]{0,1,2,3,5},
                new byte[2]{4,5},
                new byte[3]{0,4,5},
                new byte[3]{1,4,5},
                new byte[4]{0,1,4,5},
                new byte[3]{2,4,5},
                new byte[4]{0,2,4,5},
                new byte[4]{1,2,4,5},
                new byte[5]{0,1,2,4,5},
                new byte[3]{3,4,5},
                new byte[4]{0,3,4,5},
                new byte[4]{1,3,4,5},
                new byte[5]{0,1,3,4,5},
                new byte[4]{2,3,4,5},
                new byte[5]{0,2,3,4,5},
                new byte[5]{1,2,3,4,5},
                new byte[6]{0,1,2,3,4,5}
            };
        
    }        
    }
}