﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace utility
{
    static public class math_cubes8_faces
    {
        static public readonly byte max = 11;
        static public readonly ulong[] fs = new ulong[] {
            (ulong)System.Math.Pow(max+1, 0),
            (ulong)System.Math.Pow(max+1, 1),
            (ulong)System.Math.Pow(max+1, 2),
            (ulong)System.Math.Pow(max+1, 3),
            (ulong)System.Math.Pow(max+1, 4),
            (ulong)System.Math.Pow(max+1, 5),
            (ulong)System.Math.Pow(max+1, 6),
            (ulong)System.Math.Pow(max+1, 7)
        };

        static public ulong МассивГранейКубовВЧисло(this byte[] fs8)
        {
            return fs8.get_id_max(fs);
        }
        static public List<byte> МассивГранейКубовВЧисло(this ulong id)
        {
            return id.get_m_max(fs);
        }

    }
}