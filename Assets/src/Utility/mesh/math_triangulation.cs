﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using utility.go;

namespace utility
{
    static public class math_triangulation
    {
        static public Mesh triangle(this Vector3 v0, Vector3 v1, Vector3 v2)
        {
            return new Mesh
            {
                vertices = new[] { v0, v1, v2 },
                triangles = new[] { 0, 1, 2 },
                uv = new[] {
                new Vector2(0, 1),
                new Vector2(1, 1) ,
                new Vector2(1, 0)
            },
                colors = new[] {
                Color.Lerp(Color.red, Color.green, v0.y),
                Color.Lerp(Color.red, Color.green, v1.y),
                Color.Lerp(Color.red, Color.green, v2.y)
            },
                normals = new[] { v0.normalized, v1.normalized, v2.normalized },
            };
        }
        static public Mesh triangle(this Vector3 v0, Vector3 v1, Vector3 v2, Vector2 uv0, Vector2 uv1, Vector2 uv2)
        {
            return new Mesh
            {
                vertices = new[] { v0, v1, v2 },
                triangles = new[] { 0, 1, 2 },
                uv = new[] {uv0,uv1,uv2},
                //colors = new[] {
                //Color.Lerp(Color.red, Color.green, v0.y),
                //Color.Lerp(Color.red, Color.green, v1.y),
                //Color.Lerp(Color.red, Color.green, v2.y)//},
                //normals = new[] { v0.normalized, v1.normalized, v2.normalized },
            };
        }

        static public byte ИндексМассива { 
            get{
            byte i = 0;

                foreach (Transform outcome in task.triangulation.editor.world.Редактор.transform)
                    foreach (Transform child in outcome)
                    {
                        var w = child.GetComponent<task.triangulation.vertex.world>();
                        Renderer r = w.gameObject.GetComponent<Renderer>();

                        if (w.Активный)
                        {
                            i += (byte)Mathf.Pow(2, System.Convert.ToByte(child.gameObject.name));
                            r.material = Resources.Load("materials/white", typeof(Material)) as Material;
                        }
                        else
                            r.material = Resources.Load("materials/black", typeof(Material)) as Material;


                    }

                return i;
            }
        }

        static public Mesh triangle(Vector3 v0,Vector3 v1,Vector3 v2, Vector3 v)
        {
            var mesh = new Mesh
            {
                vertices = new[] { v + v0, v + v1, v + v2 },
                triangles = new[] { 0, 1, 2},
            };
            return mesh;
        }   

        static public void Показать()
        {
            ИндексМассива.Показать();
        }



        static public void Показать(this byte index)
        {
            Mesh M = null;//Собрать(index,Vector3.zero);
            if (M == null) return;     
            GameObject go = GameObject.FindGameObjectWithTag("outcome");
            M.ПривязатьМешКОбъекту(go);
        }

        static public Mesh ПостроитьКуб(this Vector3 dv)
        {
            byte index = 0;
            for (byte i=0;i<8;i++)
            {
                if (!task.point.world.PS.ContainsKey(task.point.world.Координата(dv + 0.5f * simple.меш.ВершиныКуба[i]))) continue;
                index += (byte)Mathf.Pow(2,i);
            }
            if (index == 0) return null;
            return null;//math_cell.Собрать(index, dv);
        }

        static public Mesh ПостроитьРегион()
        {
            List<Mesh> ms = new List<Mesh>();
            Mesh m;
            for (byte x = 0; x < task.Region.block.math.D; x++)
                for (byte y = 0; y < task.Region.block.math.D; y++)
                    for (byte z = 0; z < task.Region.block.math.D; z++)
                    {
                        m = new Vector3(x, y, z).ПостроитьКуб();
                        if (m == null) continue;
                        ms.Add(new Vector3(x, y, z).ПостроитьКуб());
                    }

            var combine = new CombineInstance[ms.Count];
            for (byte i = 0; i < ms.Count; i++)
                combine[i].mesh = ms[i];
            var mesh = new Mesh();
            mesh.CombineMeshes(combine, true, false);
            return mesh;
        }

        static public void ТестовыйРегион()
        {
            Mesh M = ПостроитьРегион();
            if (M == null) return;
            GameObject go = GameObject.FindGameObjectWithTag("outcome");
            M.ПривязатьМешКОбъекту(go);
        }

        

    }
}
