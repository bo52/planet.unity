﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;

static public class math_color_hit
{
    static public RaycastHit HIT;
    static GameObject obj;
    static public Vector3 КоординатаАтома;
    static public bool Луч()
    {
        Ray ray = HandleUtility.GUIPointToWorldRay(Event.current.mousePosition);

        if (!Physics.Raycast(ray, out HIT)) return false;
        if (HIT.collider == null) return false;

        obj = HIT.collider.gameObject;
        return true;
    }

    static public bool ПуститьЛуч()
    {
        if (!Луч()) return false;
        if (obj == null) return false;
        Вычисление();
        return true;
    }
    static public void Вычисление()
    {
        GameObject atom = GameObject.Find("atom3");
        if (atom == null)
        {
            atom = Object.Instantiate(Resources.Load("materials/atom3", typeof(GameObject)), Vector3.zero, Quaternion.identity) as GameObject;
            atom.name = "atom3";
        }
        atom.transform.position = HIT.point;
        //move_atom(atom);
        //КоординатаАтома = calc_atom(atom); 
            //- ЛокальныйМИР * Mathf.Pow(2, SIZE);
    }
    static public Vector3 calc_atom_zero {
        get
        {
            float k = 0;
            for (int i = 0; i < SIZE - 1; i++)
                k += 0.25f * 1 / Mathf.Pow(2, i);
            return -Vector3.one * k;
        }
    }
    static public Vector3 calc_atom(GameObject atom)
    {
            float pow = Mathf.Pow(2, SIZE);
            return 0.5f * pow * (atom.transform.localPosition - HIT.normal / pow - calc_atom_zero + ЛокальныйМИР);
    }
    static byte SIZE = 6;
    static public int МинимальноеЧисло(this float X)
    {
        int k = X < 0 ? -1 : 1;
        int xk = (int)X + k;
        float dx_back = Mathf.Abs(Mathf.Abs(xk) - Mathf.Abs(X));
        int x = (int)X;
        float dx = Mathf.Abs(Mathf.Abs(x) - Mathf.Abs(X));
        float min = Mathf.Min(dx_back, dx);
        if (min == dx_back) return xk;
        if (min == dx) return x;
        return x;
    }

    static public Vector3 ОкруглитьВектор(this Vector3 v)
    {
        return new Vector3(v.x.МинимальноеЧисло(), v.y.МинимальноеЧисло(), v.z.МинимальноеЧисло());
    }
    static public Vector3 ЛокальныйМИР {
        get
        {
            return (HIT.point - obj.transform.position - 1 / Mathf.Pow(2, SIZE) * HIT.normal).ОкруглитьВектор();
        }
    }
    static public Vector3 ЛокальныйАтом {
        get
        {
            Vector3 v = HIT.point - obj.transform.position - ЛокальныйМИР;
            v += 0.5f * Vector3.one;
            return v;
        }
    }
    static public Vector3 localScale
    {
        get
        {
            float k = 0.5f;
            if (HIT.normal == Vector3.back) return new Vector3(1, 1, k);
            if (HIT.normal == Vector3.forward) return new Vector3(1, 1, k);
            if (HIT.normal == Vector3.down) return new Vector3(1, k, 1);
            if (HIT.normal == Vector3.up) return new Vector3(1, k, 1);
            if (HIT.normal == Vector3.right) return new Vector3(k, 1, 1);
            if (HIT.normal == Vector3.left) return new Vector3(k, 1, 1);
            return Vector3.one;
        }
    }
    static public string[] size_options = new string[] { "0", "1", "2", "4", "8", "16", "32" };
    static public int get_size_int {
        get
        {
            return System.Convert.ToInt32(size_options[SIZE]);
        }
    }
    static public float get_size {
        get
        {
            return 1 / (float)get_size_int;
        }
    }
    static public Vector3 localSclale_size {
        get
        {
            return localScale * get_size;
        }
    }
    static void move_atom(GameObject atom)
    {
        atom.transform.SetParent(obj.transform.parent);
        atom.transform.position = obj.transform.position + ЛокальныйМИР + 0.5f * HIT.normal;
        atom.transform.localScale = localSclale_size;
        atom.transform.position += 0.005f * HIT.normal;

        Vector3 v = HIT.normal;
        v = new Vector3(Mathf.Abs(v.x), Mathf.Abs(v.y), Mathf.Abs(v.z));
        v -= Vector3.one;
        float part = 0.25f;
        for (int i = SIZE; i > 1; i--)
        {
            atom.transform.position += part * v;
            part *= 0.5f;
        }
        Vector3 v1 = ЛокальныйАтом * Mathf.Pow(2, SIZE - 1);
        v1.x = v.x == 0 ? 0 : (int)v1.x;
        v1.y = v.y == 0 ? 0 : (int)v1.y;
        v1.z = v.z == 0 ? 0 : (int)v1.z;
        v1 /= Mathf.Pow(2, SIZE - 1);

        atom.transform.position += v1;
        v = atom.transform.position;
        if (HIT.normal.x != 0) atom.transform.position = new Vector3(HIT.point.x, v.y, v.z);
        if (HIT.normal.y != 0) atom.transform.position = new Vector3(v.x, HIT.point.y, v.z);
        if (HIT.normal.z != 0) atom.transform.position = new Vector3(v.x, v.y, HIT.point.z);
    }
}
