﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace utility
{
    public class math_color
    {
        static public Color c = Color.white;
        static public byte color_int = 7;
        static public void ИзменитьЦвет(byte i)
        {
            color_int = i;
            c = hue_color[i];
        }
        static public byte bright = byte.MaxValue;
        static public bool drag = false;

        static public Color[] hue_color = new Color[] {
        Color.red,
        Color.green,
        Color.blue,
        Color.yellow,
        Color.cyan,
        Color.magenta,
        Color.gray,
        Color.white,
        Color.white,
        new Color(165/255.0f, 130 / 255.0f, 82 / 255.0f, 1)
    };
        static public string[] color_options = new string[] {
        "red",
        "green",
        "blue",
        "yellow",
        "cyan",
        "magenta",
        "gray",
        "white",
        "texture",
        "ground"
    };
        static public bool Яркость()
        {
            float h = 0;
            float s = 0;
            float v = 0;

            Color.RGBToHSV(c, out h, out s, out v);
            v = bright / 255.0f;

            c = Color.HSVToRGB(h, s, v);
            return true;
        }
        static public bool визуализация_цвет(bool b_color)
        {
            return b_color && (GUI.Цвет(ref c) || GUI.SelectionGrid(ref color_int, color_options));
        }
        static public bool визуализация(bool b_color)
        {
            GUILayout.BeginVertical();
            GUILayout.BeginHorizontal();
            GUI.логический(ref drag, "Drag", 10);
            GUILayout.EndHorizontal();
            GUILayout.BeginHorizontal();
            GUILayout.Label("x="+(int)(32 * math_color_hit.HIT.point.x));
            GUILayout.Label("y=" + (int)(32 * math_color_hit.HIT.point.y));
            GUILayout.Label("z=" + (int)(32 * math_color_hit.HIT.point.z));
            GUILayout.EndHorizontal();
            GUILayout.EndVertical();

            if (GUI.ползунок(ref bright, 0, 255) || визуализация_цвет(b_color))
            {
                c = hue_color[color_int];
                Яркость();
                return true;
            }

            return false;
        }
        static public void ВыполнитьТест()
        {
            if (визуализация(true))
            {
                //
            }
        }

        static public Texture2D test_tex_obj = Resources.Load("textures/1024_COLOR", typeof(Texture)) as Texture2D;
    }
}
