﻿using UnityEditor;
using UnityEngine;
using System.IO;
using utility.go;
namespace utility
{
    static public class math_texture
    {
        static public void СохранитьТекстуру(this Texture2D tex, string f = "")
        {
            if (f == "") f = Application.dataPath + "/Resources/test";
            byte[] bytes = tex.EncodeToPNG();
            FileStream file = new FileStream(f + "_COLOR.png", FileMode.Create);
            BinaryWriter binary = new BinaryWriter(file);
            binary.Write(bytes);
            file.Close();
        }

        static public void gui(ref Texture2D tex_obj)
        {
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.BeginVertical();

            tex_obj = (Texture2D)EditorGUILayout.ObjectField(tex_obj, typeof(Object));
            if (tex_obj != null)
                GUILayout.Box(tex_obj, GUILayout.Width(256), GUILayout.Height(256));
            EditorGUILayout.EndVertical();

            EditorGUILayout.BeginVertical();
            string f = Application.dataPath + "/Resources/" + objects.mat_DIRS;

            string[] files = Directory.GetFiles(f, "*_COLOR.png"); // список всех png файлов в директории C:\temp
            for (int i = 0; i < files.Length; i++)
            {
                string s = files[i].ToString().Substring(f.Length);
                s = s.Substring(0, s.IndexOf("_"));

                if (GUILayout.Button(s, new GUIStyle(), GUILayout.Width(50)))
                {
                    tex_obj = Resources.Load(objects.mat_DIRS + s + "_COLOR", typeof(Texture)) as Texture2D;
                };
            }
            EditorGUILayout.EndVertical();
            EditorGUILayout.EndHorizontal();

        }

        static public byte test_tex_f = 1;
        static public Texture2D test_tex_obj = Resources.Load(objects.mat_DIRS[1] + test_tex_f + "_COLOR", typeof(Texture)) as Texture2D;
        static public int max_pix = 32;
        static public void ИзменитьТекстуру_test()
        {
            float xt = math_color_hit.HIT.point.x / (task.test.texture.world.v2.x - task.test.texture.world.v0.x);
            float zt = math_color_hit.HIT.point.z / (task.test.texture.world.v1.z);

            string[] row_xt = xt.ToString().Split(',');
            string[] row_zt = zt.ToString().Split(',');

            int X = (int)(max_pix * System.Convert.ToDouble(row_zt[0]));
            int Y = (int)(max_pix * System.Convert.ToDouble(row_xt[0]));

            int x = (int)(max_pix * System.Convert.ToDouble("0,"+ row_zt[1]));
            int y = (int)(max_pix * System.Convert.ToDouble("0," + row_xt[1]));

            Debug.Log(row_xt[0]+"="+ row_zt[0]+":"+ x + "=" + y);

            ИзменениеТекстуры(X + x, Y + y);
        }
        static public void ИзменениеТекстуры(int x,int y)
        {
            test_tex_obj.SetPixel(y, x, math_color.c);
            test_tex_obj.Apply();
            byte[] bytes = test_tex_obj.EncodeToPNG();
            FileStream file = new FileStream(Application.dataPath + "/Resources/" + objects.mat_DIRS[1] + test_tex_f + "_COLOR.png", FileMode.Create);
            BinaryWriter binary = new BinaryWriter(file);
            binary.Write(bytes);
            file.Close();
        }

        static public void ИзменитьТекстуру(this Texture2D tex, int x, int y, Color c)
        {
            tex.SetPixel(x, y, c);
            tex.Apply();
            tex.СохранитьТекстуру();
        }
    }
}
