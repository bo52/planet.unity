﻿using UnityEngine;
using System.Collections.Generic;
namespace utility
{
    static public class Face
    {
        /// <summary>
        /// развертка по типу куб по 6 направлениям и сохранение новой вершины куба в vertex
        /// </summary>
        /// <param name="F">грань куба из шести граней</param>
        /// <param name="v">вектор-вершина куба</param>
        /// <returns></returns>
        static public Vector2 uv(this byte F, Vector3 v)
        {
            switch (F)
            {
                case 0://left
                case 1://right
                    return new Vector2(v.z, v.y);
                case 2://down
                case 3://up
                    return new Vector2(v.x, v.z);
                case 4://back
                case 5://front
                    return new Vector2(v.x, v.y);
            }
            return Vector2.zero;
        }

        //Сопоставить Вершине 0-7 Куба с Гранью 0-5
        static public bool ЭтоВнешняяГрань(this sbyte f,byte ИндексВершины)
        {
            switch (ИндексВершины)
            {
                case 0: return (f == 0 || f == 4 || f == 2);
                case 1: return (f == 1 || f == 4 || f == 2);
                case 2: return (f == 0 || f == 4 || f == 3);
                case 3: return (f == 1 || f == 4 || f == 3);
                case 4: return (f == 0 || f == 5 || f == 2);
                case 5: return (f == 1 || f == 5 || f == 2);
                case 6: return (f == 0 || f == 5 || f == 3);
                case 7: return (f == 1 || f == 5 || f == 3);
            }
            return false;
        }
    }
}