﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace utility
{
    static public class ulong_id
    {
        /// <summary>
        /// вектор конвертировать в идентификатор
        /// </summary>
        /// <param name="v">вектор для конвертирования</param>
        /// <param name="min">граница - минимальная положительная целая координата (от 0,1...255 без знака минус)</param>
        /// <param name="max">граница - максимальная положительная целая координата(от 1,2...255 и т.д)</param>
        /// <returns>идентификатор в диапазоне (0...18446744073709551615)</returns>
        static double pow(this int n, int max)
        {
            return System.Math.Pow(max+1, n);
        }

        static public ulong get_id_max(this byte[] L, ulong []fs)
        {
            ulong id = 0;
            for (var n = 0; n < fs.Length; n++)
            {
                id += fs[n] * L[n];              
            }
            return id;
        }

        static public List<byte> get_m_max(this ulong id, ulong []fs)
        {
            List<byte> L = new List<byte>();
            for (int n = fs.Length - 1; n >= 0; n--)
            {            
                L.Insert(0, (byte)(id / fs[n]));
                id -= fs[n] * L[0];
            }
            return L;
        }
        static public void test()
        {
            byte space = 8;
            byte max = 11;
            byte[] BS = new byte[] { max, max, max, max, max, max, max, max };
            //byte[] BS = new byte[] { 2, 3, 1 };
            ulong id = BS.get_id_max(math_cubes8_faces.fs);
            List<byte>bs = id.get_m_max(math_cubes8_faces.fs);
            Debug.Log(id);
            for (byte i=0;i< space; i++)
                Debug.Log(i+"="+bs[i].ToString()+"="+ BS[i].ToString());
        }
    }
}
