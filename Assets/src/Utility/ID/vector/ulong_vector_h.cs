﻿using UnityEngine;
/// <summary>
/// универсальный идентификатор вектора
/// </summary>
static public class ulong_vector_h
{
    static private ulong MAX(this uint h, uint max)
    {
        return (ulong)h * max;
    }
    static private ulong mx(this uint h, uint max)
    {
        return h.MAX(max) + 1;
    }
    static private ulong my(this uint h, uint max)
    {
        return h.mx(max) + h.mx(max) * h.MAX(max);
    }
    /// <summary>
    /// вектор конвертировать в идентификатор
    /// </summary>
    /// <param name="v">вектор для конвертирования</param>
    /// <param name="h">на сколько координата делится h=1->целый,h=2->две части или по 0.5f,h=4->четыре части или по 0.25f,h=8->восемь частей или по 0.125f и т.д.) частей</param>
    /// <param name="min">граница - минимальная положительная целая координата (от 0,1...n без знака минус)</param>
    /// <param name="max">граница - максимальная положительная целая координата(от 1,2...n и т.д)</param>
    /// <returns>идентификатор в диапазоне (0...18446744073709551615)</returns>
    static public ulong get_id_h(this Vector3 v, uint h, uint max)
    {
        v.x *= h;
        v.y *= h;
        v.z *= h;
        return (ulong)(v.x) + h.mx(max) * (ulong)(v.y) + h.my(max) * (ulong)(v.z);
    }
    static public Vector3 get_v_h(this ulong id, uint h, uint max)
    {
        ulong _mx = h.mx(max);
        ulong _my = h.my(max);

        ulong z = id / _my;
        ulong y = (id - _my * z) / _mx;
        ulong x = id - _mx * y - _my * z;
        float X = (float)x / h;
        float Y = (float)y / h;
        float Z = (float)z / h;
        return new Vector3(X, Y, Z);
    }
    static public void test()
    {
        uint h = 120;
        uint max = 5;

        Vector3 v = new Vector3(1.0f, 1.0f, 1.0f);
        ulong id = v.get_id_h(h, max);
        Vector3 V = id.get_v_h(h, max);
        Debug.Log(id + ":(" + v.x + "," + v.y + "," + v.z + ")=(" + V.x + "," + V.y + "," + V.z + ")");
    }
}
