﻿using UnityEngine;
namespace utility
{
    static public class uint_vector_scale
    {
        /// <summary>
        /// вектор конвертировать в идентификатор
        /// </summary>
        /// <param name="v">вектор для конвертирования</param>
        /// <param name="min">граница - минимальная положительная целая координата (от 0,1...255 без знака минус)</param>
        /// <param name="max">граница - максимальная положительная целая координата(от 1,2...255 и т.д)</param>
        /// <returns>идентификатор в диапазоне (0...18446744073709551615)</returns>
        static public uint get_id(this Vector3 v, byte scale)
        {
            return (uint)(v.x + scale + (2*scale + 1) * (v.y + scale) + Mathf.Pow(2*scale + 1, 2) * (v.z + scale));
        }
        static public Vector3 get_v(this uint id, byte scale)
        {
            float _mx = 2*scale + 1;
            float _my = Mathf.Pow(_mx, 2);

            uint z = (uint)(id / _my);
            uint y = (uint)((id - _my * z) / _mx);
            uint x = (uint)(id - _mx * y - _my * z);
            return new Vector3(x, y, z) - scale * Vector3.one;
        }
        static public void test(Vector3 v)
        {
            byte scale = 5;

            uint id = v.get_id(scale);
            Vector3 V = id.get_v(scale);
            Debug.Log(id + ":(" + v.x + "," + v.y + "," + v.z + ")=(" + V.x + "," + V.y + "," + V.z + ")");
        }
    }
}
