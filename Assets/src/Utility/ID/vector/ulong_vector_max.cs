﻿using UnityEngine;
namespace utility
{
    static public class ulong_vector_max
    {
        /// <summary>
        /// вектор конвертировать в идентификатор
        /// </summary>
        /// <param name="v">вектор для конвертирования</param>
        /// <param name="min">граница - минимальная положительная целая координата (от 0,1...255 без знака минус)</param>
        /// <param name="max">граница - максимальная положительная целая координата(от 1,2...255 и т.д)</param>
        /// <returns>идентификатор в диапазоне (0...18446744073709551615)</returns>
        static public ulong get_id_MAX(this Vector3 v, byte max)
        {
            ulong id = (ulong)v.x;
            id+= (ulong)((max + 1) * v.y);
            id+= (ulong)(Mathf.Pow(max + 1, 2) * v.z); 
            return id;
        }
        static public Vector3 get_v_MAX(this ulong id, byte max)
        {
            ulong _mx = (ulong)(max + 1);
            ulong _my = (ulong)Mathf.Pow(_mx, 2);

            ulong z = id / _my;
            ulong y = (id - _my * z) / _mx;
            ulong x = id - _mx * y - _my * z;
            return new Vector3(x, y, z);
        }
        static public void test(Vector3 v)
        {
            byte max = 5;

            ulong id = v.get_id_MAX(max);
            Vector3 V = id.get_v_MAX(max);
            Debug.Log(id + ":(" + v.x + "," + v.y + "," + v.z + ")=(" + V.x + "," + V.y + "," + V.z + ")");
        }
    }
}
