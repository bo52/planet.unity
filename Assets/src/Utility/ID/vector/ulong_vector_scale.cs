﻿using UnityEngine;
namespace utility
{
    static public class ulong_vector_scale
    {
        /// <summary>
        /// вектор конвертировать в идентификатор
        /// </summary>
        /// <param name="v">вектор для конвертирования</param>
        /// <param name="min">граница - минимальная положительная целая координата (от 0,1...255 без знака минус)</param>
        /// <param name="max">граница - максимальная положительная целая координата(от 1,2...255 и т.д)</param>
        /// <returns>идентификатор в диапазоне (0...18446744073709551615)</returns>
        static public ulong GET_ID_SCALE(this Vector3 v, byte scale)
        {
            return (ulong)(v.x + scale + (2*scale + 1) * (v.y + scale) + Mathf.Pow(2*scale + 1, 2) * (v.z + scale));
        }
        static public Vector3 GET_V_SCALE(this ulong id, byte scale)
        {
            ulong _mx = (ulong)(2*scale + 1);
            ulong _my = (ulong)Mathf.Pow(_mx, 2);

            ulong z = (id / _my);
            ulong y = ((id - _my * z) / _mx);
            ulong x = (id - _mx * y - _my * z);
            return new Vector3(x, y, z) - scale * Vector3.one;
        }
        static public void test(Vector3 v)
        {
            byte scale = 4;

            ulong id = v.GET_ID_SCALE(scale);
            Vector3 V = id.GET_V_SCALE(scale);
            Debug.Log(id + ":(" + v.x + "," + v.y + "," + v.z + ")=(" + V.x + "," + V.y + "," + V.z + ")");
        }
    }
}
