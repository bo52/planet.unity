﻿using UnityEngine;
namespace utility
{
    static public class uint_vector_max
    {
        /// <summary>
        /// вектор конвертировать в идентификатор
        /// </summary>
        /// <param name="v">вектор для конвертирования</param>
        /// <param name="min">граница - минимальная положительная целая координата (от 0,1...255 без знака минус)</param>
        /// <param name="max">граница - максимальная положительная целая координата(от 1,2...255 и т.д)</param>
        /// <returns>идентификатор в диапазоне (0...18446744073709551615)</returns>
        static public uint get_id_max(this Vector3 v, byte max)
        {
            return (uint)(v.x + (max + 1) * v.y + Mathf.Pow(max + 1, 2) * v.z );
        }
        static public Vector3 get_v_max(this uint id, byte max)
        {
            float _mx = max + 1;
            float _my = Mathf.Pow(_mx, 2);

            uint z = (uint)(id / _my);
            uint y = (uint)((id - _my * z) / _mx);
            uint x = (uint)(id - _mx * y - _my * z);
            return new Vector3(x, y, z);
        }
        static public void test(Vector3 v)
        {
            byte max = 5;

            uint id = v.get_id_max(max);
            Vector3 V = id.get_v_max(max);
            Debug.Log(id + ":(" + v.x + "," + v.y + "," + v.z + ")=(" + V.x + "," + V.y + "," + V.z + ")");
        }
    }
}
