﻿using UnityEngine;
/// <summary>
/// универсальный идентификатор вектора
/// </summary>
static public class ulong_vector3
{
    static private ulong MAX(this uint h, uint min, uint max)
    {
        return (ulong)h * (max + min);
    }
    static private ulong mx(this uint h, uint min, uint max)
    {
        return h.MAX(min, max) + 1;
    }
    static private ulong my(this uint h, uint min, uint max)
    {
        return h.mx(min, max) + h.mx(min, max) * h.MAX(min, max);
    }
    /// <summary>
    /// вектор конвертировать в идентификатор
    /// </summary>
    /// <param name="v">вектор для конвертирования</param>
    /// <param name="h">на сколько координата делится h=1->целый,h=2->две части или по 0.5f,h=4->четыре части или по 0.25f,h=8->восемь частей или по 0.125f и т.д.) частей</param>
    /// <param name="min">граница - минимальная положительная целая координата (от 0,1...n без знака минус)</param>
    /// <param name="max">граница - максимальная положительная целая координата(от 1,2...n и т.д)</param>
    /// <returns>идентификатор в диапазоне (0...18446744073709551615)</returns>
    static public ulong GET_ID_MAX_MIN(this Vector3 v, uint h, uint min, uint max)
    {
        v.x += min;
        v.y += min;
        v.z += min;
        v.x *= h;
        v.y *= h;
        v.z *= h;
        return (ulong)(v.x) + h.mx(min, max) * (ulong)(v.y) + h.my(min, max) * (ulong)(v.z);
    }
    static public Vector3 GET_V_MAX_MIN(this ulong id, uint h, uint min, uint max)
    {
        ulong _mx = h.mx(min, max);
        ulong _my = h.my(min, max);

        ulong z = id / _my;
        ulong y = (id - _my * z) / _mx;
        ulong x = id - _mx * y - _my * z;
        float X = (float)x / h - min;
        float Y = (float)y / h - min;
        float Z = (float)z / h - min;
        return new Vector3(X, Y, Z);
    }
    static public void test()
    {
        uint h = 1000;
        uint min = 5;
        uint max = 5;

        Vector3 v = new Vector3(1.123f, -0.374f, 1.855f);
        ulong id = v.GET_ID_MAX_MIN(h, min, max);
        Vector3 V = id.GET_V_MAX_MIN(h, min, max);
        Debug.Log(id + ":(" + v.x + "," + v.y + "," + v.z + ")=(" + V.x + "," + V.y + "," + V.z + ")");
    }
}
