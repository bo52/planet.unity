﻿using UnityEngine;
namespace utility
{
    static public class uint_vector_max_min
    {
        /// <summary>
        /// вектор конвертировать в идентификатор
        /// </summary>
        /// <param name="v">вектор для конвертирования</param>
        /// <param name="min">граница - минимальная положительная целая координата (от 0,1...255 без знака минус)</param>
        /// <param name="max">граница - максимальная положительная целая координата(от 1,2...255 и т.д)</param>
        /// <returns>идентификатор в диапазоне (0...18446744073709551615)</returns>
        static public uint get_id_max_min(this Vector3 v, byte min, byte max)
        {
            return (uint)(v.x + min + (max + min + 1) * (v.y + min) + Mathf.Pow(max + min + 1, 2) * (v.z + min));
        }
        static public Vector3 get_v_max_min(this uint id, byte min, byte max)
        {
            uint _mx = (uint)(max + min + 1);
            uint _my = (uint)Mathf.Pow(_mx, 2);

            uint z = id / _my;
            uint y = (id - _my * z) / _mx;
            uint x = (id - _mx * y - _my * z);
            return new Vector3(x,y,z) - min*Vector3.one;
        }
        static public void test(Vector3 v)
        {
            byte min = 5;
            byte max = 5;

            uint id = v.get_id_max_min(min, max);
            Vector3 V = id.get_v_max_min(min, max);
            Debug.Log(id + ":(" + v.x + "," + v.y + "," + v.z + ")=(" + V.x + "," + V.y + "," + V.z + ")");
        }
    }
}
