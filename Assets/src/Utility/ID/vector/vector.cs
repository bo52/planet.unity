﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace utility
{
    static public class vector
    {
        static public Vector3 ABS(this Vector3 v)
        {
            return new Vector3(Mathf.Abs(v.x), Mathf.Abs(v.y), Mathf.Abs(v.z));
        }

        static public Vector3 Целое(this Vector3 v)
        {
            //Ceiling
            //+7.50f->8.0f
            //+7.01f->8.0f
            //+0.01f->1.0f
            //-0.01f->0.0f
            Vector3 abs = new Vector3(Mathf.Abs(v.x), Mathf.Abs(v.y), Mathf.Abs(v.z));
            abs.x = (float)System.Math.Ceiling(abs.x);
            abs.y = (float)System.Math.Ceiling(abs.y);
            abs.z = (float)System.Math.Ceiling(abs.z);
            if (v.x < 0)
                abs.x *= -1;
            else
                abs.x -= 1;
            if (v.y < 0)
                abs.y *= -1;
            else
                abs.y -= 1;
            if (v.z < 0)
                abs.z *= -1;
            else
                abs.z -= 1;
            return abs;
        }
        static public Vector3 КоординатаРегионаОтГлобальной(this Vector3 p)
        {
            p /= task.Region.block.math.D;
            return p.Целое();
        }
        static public int МинимальноеЧисло(this float X)
        {
            int k = X < 0 ? -1 : 1;
            int xk = (int)X + k;
            float dx_back = Mathf.Abs(Mathf.Abs(xk) - Mathf.Abs(X));

            int x = (int)X;
            float dx = Mathf.Abs(Mathf.Abs(x) - Mathf.Abs(X));
            float min = Mathf.Min(dx_back, dx);

            if (min == dx_back) return xk;
            if (min == dx) return x;
            return x;
        }
        static public Vector3 ОкруглитьВектор(this Vector3 v)
        {
            return new Vector3(v.x.МинимальноеЧисло(), v.y.МинимальноеЧисло(), v.z.МинимальноеЧисло());
        }

        static public Vector3 Нормаль(this Vector3 v, Vector3 A, Vector3 B)
        {

            Vector3 Side1 = new Vector3(A.x - v.x, A.y - v.y, A.z - v.z);
            Vector3 Side2 = new Vector3(B.x - A.x, B.y - A.y, B.z - A.z);
            Vector3 Perp = Vector3.Cross(Side1, Side2);
            float perpLength = Perp.magnitude;
            return Perp/ perpLength;
        }

    }
}
