﻿using UnityEngine;
namespace utility
{
    static public class ulong_vector
    {
        /// <summary>
        /// вектор конвертировать в идентификатор
        /// </summary>
        /// <param name="v">вектор для конвертирования</param>
        /// <param name="min">граница - минимальная положительная целая координата (от 0,1...255 без знака минус)</param>
        /// <param name="max">граница - максимальная положительная целая координата(от 1,2...255 и т.д)</param>
        /// <returns>идентификатор в диапазоне (0...18446744073709551615)</returns>
        static public ulong GET_ID(this Vector3 v, uint min, uint max)
        {
            return (ulong)(v.x + min + (max + min + 1) * (v.y + min) + Mathf.Pow(max + min + 1, 2) * (v.z + min));
        }
        static public Vector3 GET_V(this ulong id, uint min, uint max)
        {
            ulong _mx = max + min + 1;
            ulong _my = (ulong)Mathf.Pow(_mx, 2);

            ulong z = (id / _my);
            ulong y = ((id - _my * z) / _mx);
            ulong x = (id - _mx * y - _my * z);
            return new Vector3(x, y, z) - min * Vector3.one;
        }
        static public void test(Vector3 v)
        {
            byte min = 5;
            byte max = 5;

            ulong id = v.GET_ID(min, max);
            Vector3 V = id.GET_V(min, max);
            Debug.Log(id + ":(" + v.x + "," + v.y + "," + v.z + ")=(" + V.x + "," + V.y + "," + V.z + ")");
        }
    }
}
