﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;
using System.Linq;
namespace oop
{
    static public class Chunk_funs
    {
        #region СкалаВершиныЧанка
        static public float get_R(this sbyte Скала)
        {
            return Mathf.Pow(2, Скала - 1);
        }
        #endregion
    }

    public class Chunk
    {
        public CHANK p;
        public Chunk(CHANK p)
        {
            this.p = p;
        }

        byte DET
        {
            get
            {
                return (byte)Mathf.Sqrt(cube.Count);
            }
        }
        public List<Vertex> cube = new List<Vertex>();        
        List<Chunk> sub = new List<Chunk>();
        List<Chunk> div = new List<Chunk>();
        #region КоординатаВершиныЧанка
        static public uint MAX = 8096;
        static public uint MAXX = (uint)(1.5f * MAX);
        static public byte H = 16;
        static public ulong Координата(Vector3 v)
        {
            return v.GET_ID_MAX_MIN(H, MAXX, MAXX);
        }
        static public Vector3 Координата(ulong id)
        {
            return id.GET_V_MAX_MIN(H, MAXX, MAXX);
        }
        #endregion
        public sbyte scale = -2;
        public Vector3 Центр;
        public float R
        {
            get
            {
                return scale.get_R();
            }
        }
    }
}
