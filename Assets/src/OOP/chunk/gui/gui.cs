﻿using UnityEngine;
using UnityEditor;

namespace oop.chunk
{
    [CustomEditor(typeof(World))]
    public class gui : Empty.gui
    {
        //при клике привязанного игрового объекта
        public gui()
        {
        }

        public override void OnSceneGUI()
        {
            base.OnSceneGUI();
            ((World)target).ПоказатьВершиныЧанков();
        }


        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
        }
    }
}
