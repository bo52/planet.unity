﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
namespace oop.chunk
{
    [ExecuteInEditMode]
    public class World : simple.world
    {
        private void Start()
        {
            Debug.Log("strart");
        }
        public string дир;
        public string файл;

        public CHANK ЧАНК
        {
            get
            {
                return Node.НайтиЧанк(файл);

            }
        }

        public bool gui3()
        {
            GUILayout.BeginHorizontal();
            if (utility.GUI.кнопка("Перестроить",100))
            {
                ЧАНК.построить();
            }
            if (utility.GUI.кнопка("Сбросить", 100))
            {
                ЧАНК.построить(true);
            }
            if (utility.GUI.кнопка("Сохранить", 100))
            {
                ЧАНК.Сохранить();
            }
            if (utility.GUI.кнопка("Загрузить", 100))
            {
                ЧАНК.Загрузить();
                ЧАНК.построить();
            }
            GUILayout.EndHorizontal();
            return true;
        }
        public override void СозданиеУзлов()
        {
            base.СозданиеУзлов();
            ДобавитьУзел("УправлениеЧанком", gui3).showPosition=true;
        }
    }
}
