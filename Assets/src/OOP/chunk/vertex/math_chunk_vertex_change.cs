﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Linq;
static public class math_chunk_vertex_change
{
    static readonly Vector3[] Соседи = new Vector3[]
    {
        new Vector3(-1,-1,-1),
        new Vector3(0,-1,-1),
        new Vector3(+1,-1,-1),
        new Vector3(-1,-1,+1),
        new Vector3(0,-1,1),
        new Vector3(+1, -1, 1),
        new Vector3(-1,0,1),
        new Vector3(0,0,1),
        new Vector3(+1, 0, 1),
        new Vector3(-1,+1,1),
        new Vector3(0,+1,1),
        new Vector3(+1, +1, 1),
        new Vector3(+1,-1,0),
        new Vector3(+1,0,0),
        new Vector3(+1, +1, 0),
        new Vector3(+1,0,-1),
        new Vector3(+1, +1, -1),
        new Vector3(0,+1,0),
        new Vector3(-1,+1,0),
        new Vector3(0,+1,-1),
        new Vector3(-1,+1,-1),
        new Vector3(-1,-1,0),
        new Vector3(-1,0,-1),
    };
    static public void ДобавитьЧёрныеНаГранице(this oop.CHANK h, oop.Vertex vx)
    {
        if (vx.активный != 0) return;
        var v0 = vx.КООРДИНАТА;
        foreach (var dv in Соседи)
        {
            var в = v0 + dv;
            var ид = oop.Vertex.Координата(в);
            if (h.Найти(ид)!=null) continue;

            Handles.color = Color.black;
            var point = math_chunk_vertex.handleTransform.TransformPoint(в);
            if (Handles.Button(point, math_chunk_vertex.handleRotation, 2.5f * MeshStudy.handleSize, MeshStudy.handleSize, Handles.DotHandleCap))
            {
                var v_new=new oop.Vertex(в,h.organ);
                h.построить();
            }
        }
    }

    static public void ИзмененниеВершиныЧанка(this oop.CHANK h)
    {
        for (var n = 0; n < 3; n++)
            foreach (var vx in h.cells[n].ToArray())
            {
                Handles.color = vx.Цвет;
                h.ИзмененниеВершиныЧанка(vx);
                h.ДобавитьЧёрныеНаГранице(vx);
            }
    }

    static public void ИзмененниеВершиныЧанка(this oop.CHANK h,oop.Vertex vx)
    {
        Vector3 point=math_chunk_vertex.handleTransform.TransformPoint(vx.КООРДИНАТА);
        if (Handles.Button(point, math_chunk_vertex.handleRotation, 2.5f*MeshStudy.handleSize, MeshStudy.handleSize, Handles.DotHandleCap))
        {
            var l = h.cells[0];
            if (l.Count == 1)
                if (vx == l[0]) return;

            if (vx.активный == 0)
                h.organ.ПереместитьВершину(vx, vx.активный, 2);
            else
                h.organ.ПереместитьВершину(vx, vx.активный, 0);

            h.построить();
            //vx.активный = !vx.активный;
            //полное удаление
            //добавление неактивных вершин
        }
    }
}
