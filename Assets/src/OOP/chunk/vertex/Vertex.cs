﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using utility.go;
using utility;
namespace oop
{
    public class Vertex
    {
        public Chunk h;
        public Vector3 rVE(byte i)
        {
            return R * VE(i);
        }
        public Vector3 rVE(ushort edge)
        {
            return R * VE(edge);
        }
        public Vector3 VE(byte i)
        {
            return Ve(i) + dv[i];
        }
        public Vector3 Ve(byte i)
        {
            return КООРДИНАТА + 0.5f * math_cell_triangle.ТриОси[i];
        }
        public Vector3 КООРДИНАТА
        {
            get
            {
                return Координата(Вершина);
            }
        }
        static public Vector3 Координата(uint id)
        {
            return id.get_v_max_min(MAXX, MAXX);
        }
        static sbyte scale=1;
        static public float R
        {
            get
            {
                return oop.Chunk_funs.get_R(scale);
            }
        }

        public byte активный=0;
        public Vector3[] dv = new Vector3[3];
        public bool[] использует = new bool[3];
        public Color Цвет
        {
            get
            {
                switch (активный)
                {
                    case 0: return Color.blue;
                    case 1: return Color.gray;
                    case 2: return Color.black;
                }
                return Color.black;
            }
        }

        #region КоординатаВершиныЧанка
        static public uint MAX = 128;
        static public byte MAXX = (byte)(1.5f * MAX);
        static public byte H = 16;
        static public uint Координата(Vector3 v)
        {
            return v.get_id_max_min(MAXX, MAXX);
        }
        #endregion
        public uint Вершина;

        public Vector3 VE(ushort edge)
        {
            return VE(math_cell_triangle.РеброПоВершине[edge, 1]);
        }
        public void DoAction(oop.chunk.World w,Vector3 localPos, byte i)
        {
            var old_v = rVE(i);
            var new_v = localPos - R*Ve(i);
            if (Mathf.Abs(new_v.x) > 0.45f) return;
            if (Mathf.Abs(new_v.y) > 0.45f) return;
            if (Mathf.Abs(new_v.z) > 0.45f) return;
            //if (new_v == dv[i]) return;

            dv[i] = new_v;
            w.gameObject.МодификацияВершин(old_v, localPos);
        }
        public void Сбросить()
        {
            использует[0] = false;
            использует[1] = false;
            использует[2] = false;
        }
        public bool УДАЛИТЬ
        {
            get
            {
                if (использует[0]) return false;
                if (использует[1]) return false;
                if (использует[2]) return false;
                return true;
            }
        }
        public Vertex(uint Вершина, oop.Chunk h, byte активный=0)
        {
            this.активный = активный;
            this.Вершина = Вершина;
            this.h = h;
        }
        public Vertex(Vector3 Вершина, oop.Chunk h, byte активный = 0)
        {
            this.активный = активный;
            this.Вершина = Vertex.Координата(Вершина);
            this.h = h;
            h.p.cells[активный].Add(this);
            if (активный==0)
                h.p.organ.cube.Add(this);
        }
        public Vertex(uint Вершина, oop.Chunk h, Vector3 v0, Vector3 v1, Vector3 v2, byte активный=0)
        {
            this.активный = активный;
            this.Вершина = Вершина;
            dv[0] = v0;
            dv[1] = v1;
            dv[2] = v2;
            this.h = h;
        }
    }
}
