﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using System.Linq;

static public class math_chunk_vertex
{
    static public Transform handleTransform;//управляющий элемент
    static public Quaternion handleRotation;//угол управляющего элемента
    static public void ОбновитьУправляющийЭлемент()
    {
        handleTransform = simple.manager.world_name("StudyChunk").transform;
        handleRotation = Tools.pivotRotation == PivotRotation.Local ? handleTransform.rotation : Quaternion.identity;
    }
    static public void ПоказатьВершиныЧанков(this oop.chunk.World w)
    {
        ОбновитьУправляющийЭлемент();
        var h = w.ЧАНК;
        if (h == null) return;

        switch (simple.world.РежимСчанка)
        {
            case 0:
                w.ДвижениеВершиныЧанка(h);
                break;
            case 1:
                w.ЧАНК.ИзмененниеВершиныЧанка();
                break;

        }
    }
}
