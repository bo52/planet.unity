﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
static public class math_chunk_vertex_move
{
    static public void ДвижениеВершиныЧанка(this oop.chunk.World w, oop.CHANK h)
    {
        for (var n=0;n<3;n++)
            foreach (var vx in h.cells[n])
                for (byte i = 0; i < 3; i++)
                {
                    if (!vx.использует[i])
                        continue;
                    Handles.color = vx.Цвет;
                    Vector3 point = math_chunk_vertex.handleTransform.TransformPoint(vx.rVE(i));
                    w.ДвижениеВершиныЧанка(point,vx, i);
                }
    }

    static public void ДвижениеВершиныЧанка(this oop.chunk.World w,Vector3 point, oop.Vertex vx,byte i)
    {
        point = Handles.FreeMoveHandle(point, math_chunk_vertex.handleRotation, MeshStudy.handleSize, Vector3.zero, Handles.DotHandleCap);
        //drag
        if (GUI.changed) //1
        {
            vx.DoAction(w,math_chunk_vertex.handleTransform.InverseTransformPoint(point), i); //2
        }
    }

}
