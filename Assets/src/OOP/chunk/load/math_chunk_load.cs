﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

static public class math_chunk_load
{
    static public string DIR = Application.dataPath + "/Resources/chunk/";
    static public List<oop.chunk.Node> УзлыЧанков = new List<oop.chunk.Node>();
    static public List<oop.CHANK> чанки = new List<oop.CHANK>();

    static public void РекурсияЗагрузки(string дир, string name)
    {
        if (дир == "")
        {
            name = "chunk";
        }
        УзлыЧанков.Add(new oop.chunk.Node(дир));
        var node = УзлыЧанков[УзлыЧанков.Count-1];

        DirectoryInfo dir = new DirectoryInfo(math_chunk_load.DIR + дир);
        foreach (var item in dir.GetDirectories())
        {
            РекурсияЗагрузки(дир + item.Name + '/', item.Name);
        }

        foreach (var item in dir.GetFiles("*.ncs"))
        {
            node.Загрузить(item.Name);
        }
    }

    static public oop.CHANK Загрузить(this string дир, string файл)
    {
        var i = чанки.FindIndex(x => x.ФАЙЛ == файл);
        if (i == -1)
        {
            i = чанки.Count;
            чанки.Add(new oop.CHANK(файл, дир));
            чанки[i].Загрузить();
        }
        return чанки[i];
    }

    static public oop.CHANK Загрузить(this oop.chunk.Node УзелЧанков,string файл)
    {
        return УзелЧанков.ДИР.Загрузить(файл);
    }


    static public void Загрузить(this oop.CHANK чанк)
    {
        чанк.cells[0].Clear();
        чанк.cells[1].Clear();
        чанк.cells[2].Clear();

        string[] rows = File.ReadAllLines(DIR+чанк.ДИР + чанк.ФАЙЛ);
        uint id;
        for (int i = 0; i < rows.Length; i++)
        {
            var val=rows[i].Split(' ');
            id = System.Convert.ToUInt32(val[0]);

            var active=System.Convert.ToByte(val[1]);
            var v0 = new Vector3(System.Convert.ToSingle(val[2]), System.Convert.ToSingle(val[3]), System.Convert.ToSingle(val[4]));
            var v1 = new Vector3(System.Convert.ToSingle(val[5]), System.Convert.ToSingle(val[6]), System.Convert.ToSingle(val[7]));
            var v2 = new Vector3(System.Convert.ToSingle(val[8]), System.Convert.ToSingle(val[9]), System.Convert.ToSingle(val[10]));
            var vx = new oop.Vertex(id, чанк.organ, v0, v1, v2, active);
        }
    }
}
