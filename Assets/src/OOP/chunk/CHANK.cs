﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using utility.go;
using utility.combine;
namespace oop {
    public class CHANK
    {
        public string ФАЙЛ;
        public string ДИР;
        public Chunk organ;
        public CHANK(string ФАЙЛ, string ДИР)
        {
            this.ФАЙЛ = ФАЙЛ;
            this.ДИР = ДИР;
            organ = new Chunk(this);
        }
        public List<Vertex>[] cells = new List<Vertex>[3] {new List<Vertex>(),new List<Vertex>(),new List<Vertex>() };
        public void Сбросить()
        {
            cells[2]=cells[1].Union(cells[2]).ToList();
            cells[1].Clear();
        }
        //public void Очистить()
        //{
            //var cnt = -1;
            //foreach (var vx in cells[2].ToArray())
            //{
                //cnt++;
                //if (vx.УДАЛИТЬ) cells[2].RemoveAt(cnt);
            //}
        //}
        public void Обнулить()
        {
            cells[0].Clear();
            cells[1].Clear();
            cells[2].Clear();
            organ.cube.Clear();
        }
        public Vertex Найти(uint v)
        {
            for(var i=0; i < 3; i++){
                var t = cells[i].FindIndex(x => x.Вершина == v);
                if (t != -1) return cells[i][t];
            }
            return null;
        }

        public GameObject Оболочка
        {
            get
            {
                var l = GameObject.FindGameObjectsWithTag("StudyChunk").Where<GameObject>(p => p.name == ДИР + ФАЙЛ).ToList();
                if (l.Count == 0)
                {
                    l.Add(new GameObject());
                    l[0].name = ДИР + ФАЙЛ;
                    l[0].tag = "StudyChunk";
                    l[0].transform.parent = simple.manager.world_name("StudyChunk").transform;
                    var w = l[0].AddComponent<oop.chunk.World>();
                    w.файл = ФАЙЛ;
                    w.дир = ДИР;
                }
                return l[0];
            }
        }
        public void Показать(Dictionary<uint, List<Mesh>> меши)
        {
            var p = Оболочка;
            var w = p.GetComponent<oop.chunk.World>();

            p.transform.УдалениеОбъектов();
            foreach (KeyValuePair<uint, List<Mesh>> val in меши)
            {
                var combine = new CombineInstance[val.Value.Count];
                var cnt = 0;
                foreach (var m in val.Value)
                {
                    combine[cnt].mesh = m;
                    cnt++;
                }
                Mesh M = combine.ToMesh();
                M.ПривязатьМешКОбъекту(p.transform, 10, val.Key);
            }
        }
    }
}
