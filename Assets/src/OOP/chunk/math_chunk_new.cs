﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

static public class math_chunk_new
{
    static public void gui_НовыйЧанк()
    {
        if (utility.GUI.кнопка("Новый"))
            НовыйЧанк();
    }

    static void НовыйЧанк()
    {
        var dir = Application.dataPath + math_chunk_save.DIR + "test/";
        if (!Directory.Exists(dir))
            Directory.CreateDirectory(dir);

        var файл = funs.Уникальный + ".ncs";
        var Новый = new oop.CHANK(файл, dir);

        Новый.генератор();
        Новый.Сохранить();
        Новый.построить();
    }
}
