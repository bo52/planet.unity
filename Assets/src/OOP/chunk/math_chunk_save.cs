﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

static public class math_chunk_save
{
    static public string DIR = "/Resources/chunk/";

    static public string Строка(this oop.Vertex vx)
    {
        return
            vx.Вершина + " " +
            vx.активный + " " +
            //" " + kvp.Value.Вершина.x + " " + kvp.Value.Вершина.y + " " + kvp.Value.Вершина.z +                        
            vx.dv[0].x + " " + vx.dv[0].y + " " + vx.dv[0].z + " " +
            vx.dv[1].x + " " + vx.dv[1].y + " " + vx.dv[1].z + " " +
            vx.dv[2].x + " " + vx.dv[2].y + " " + vx.dv[2].z;
    }

    static public void Сохранить(this oop.CHANK чанк)
    {
        StreamWriter sw = new StreamWriter(Application.dataPath+ DIR+чанк.ДИР+чанк.ФАЙЛ, false);
        foreach (var vx in чанк.cells[0])
            sw.WriteLine(vx.Строка());
        foreach (var vx in чанк.cells[1])
            sw.WriteLine(vx.Строка());
        sw.Close();
    }
}
