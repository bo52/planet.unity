﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using utility;
namespace Empty
{
    public class Node
    {
        public bool showPosition;
        public string Name = "empty";
        public System.Func<bool> выполнить;

        public Node(string Name, System.Func<bool> выполнить)
        {
            this.Name = Name;
            this.выполнить = выполнить;
        }

        public void Сворачивание()
        {
            showPosition = EditorGUILayout.Foldout(showPosition, Name);
            if (showPosition)
                if (Selection.activeTransform)
                    выполнить();
            if (!Selection.activeTransform)
                showPosition = false;
        }
    }
    public class world : MonoBehaviour
    {
        public virtual void gui()
        {
            Foldout();
        }
        #region Foldout
        public List<Node> Узлы;
        public Node ДобавитьУзел(string Name, System.Func<bool> Delegate)
        {
            var i = Узлы.FindIndex(x => x.Name == Name);
            if (i == -1)
            {
                i = Узлы.Count;
                Узлы.Add(new Node(Name, Delegate));
            }
            return Узлы[i];
        }
        public void Foldout()
        {
            foreach (var node in Узлы)
            {
                node.Сворачивание();
            }
        }
        #endregion

        // Update is called once per frame
        void Update()
        {

        }
    }
}
