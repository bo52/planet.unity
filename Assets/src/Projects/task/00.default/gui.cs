﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
namespace Empty{
    [CustomEditor(typeof(world))]
    public class gui : Editor
    {

        public virtual void event_MouseMove()
        {
        }

        public virtual void event_leftMouseUp()
        {
        }
        public virtual void event_rightMouseUp()
        {
        }
        public virtual void event_middleMouseUp()
        {
        }

        public virtual void event_MouseUp()
        {
            switch (Event.current.button)
            {
                case 0:
                    event_leftMouseUp();
                    break;
                case 1:
                    event_rightMouseUp();
                    break;
                case 2:
                    event_middleMouseUp();
                    break;
            }
        }
        public virtual void event_MouseDown()
        {
            switch (Event.current.button)
            {
                case 0:
                    event_leftMouseDown();
                    break;
                case 1:
                    event_rightMouseDown();
                    break;
                case 2:
                    utility.math_color.drag = !utility.math_color.drag;
                    event_middleMouseDown();
                    break;
            }
        }
        public virtual void event_leftCtrlMouseDown()
        {
        }


        public virtual bool event_leftMouseDown()
        {
            if (Event.current.control)
            {
                event_leftCtrlMouseDown();
                return true;
            }
            return false;
        }
        public virtual void event_rightMouseDown()
        {
        }
        public virtual void event_middleMouseDown()
        {
        }
        public virtual void event_KeyDown()
        {
        }
        public virtual void event_KeyUp()
        {
        }

        public void event_InspectorGUI()
        {
            Event e = Event.current;
            int controlID = GUIUtility.GetControlID(FocusType.Passive);
            HandleUtility.AddDefaultControl(controlID);

            switch (e.GetTypeForControl(controlID))
            {
                case EventType.MouseMove:
                    event_MouseMove();
                    break;
                case EventType.MouseUp:
                    event_MouseUp();
                    break;
                case EventType.MouseDown:
                    event_MouseDown();
                    break;
                case EventType.KeyDown:
                    event_KeyDown();
                    break;
                case EventType.KeyUp:
                    event_KeyUp();
                    break;
            }
        }

        public virtual void OnSceneGUI()
        {
            Event e = Event.current;
            int controlID = GUIUtility.GetControlID(FocusType.Passive);
            HandleUtility.AddDefaultControl(controlID);

            switch (e.GetTypeForControl(controlID))
            {
                case EventType.MouseMove:
                    event_MouseMove();
                    break;
                case EventType.MouseUp:
                    event_MouseUp();
                    break;
                case EventType.MouseDown:
                    event_MouseDown();
                    break;
                case EventType.KeyDown:
                    event_KeyDown();
                    break;
                case EventType.KeyUp:
                    event_KeyUp();
                    break;
            }
            Repaint();
            task.controller.math.Проверка();
            //((simple.world)target).ЛокальныйЗапуск_Первый();
        }
        public override void OnInspectorGUI()
        {
            utility.math_color.ВыполнитьТест();
            ((simple.world)target).gui();
            ((simple.world)target).gui_ЛокальныйЗапуск();
        }
    }

}
