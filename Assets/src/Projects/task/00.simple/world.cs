﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using utility;
namespace simple
{
    public class world : Empty.world
    {

        public virtual void СозданиеУзлов()
        {
            if (Узлы != null) return;
            Узлы = new List<Empty.Node>();
            Узлы.Add(new Empty.Node("Базовые", gui1));

            var Node = new Empty.Node("МанипуляцияЧанка", gui2);
            Node.showPosition = true;
            Узлы.Add(Node);
        }
        public bool gui1()
        {
            GUILayout.BeginHorizontal();
            if (utility.GUI.кнопка("Локальный", 100))
                ЛокальныйЗапуск();
            if (utility.GUI.кнопка("Тестовый", 100))
                ТестовыйЗапуск();
            //if (utility.GUI.кнопка("Обновить", 100))
            GUILayout.EndHorizontal();
            return true;
        }
        #region Настройки
        static public byte РежимСчанка = 1;
        static public string[] opt_chunk = new string[] { "move", "create" };
        public bool gui2()
        {
            return РежимСчанка.SelectionGrid(opt_chunk);
        }
        #endregion

        public override void gui()
        {
            СозданиеУзлов();
            base.gui();
        }

        #region Первый
        public bool ЛокальныйПервый = true;
        public virtual void ЛокальныйЗапуск()
        {
        }
        public virtual void ТестовыйЗапуск()
        {
            task.controller.world.test();
        }
        public void ЛокальныйЗапуск_Первый()
        {
            if (ЛокальныйПервый)
            {
                ЛокальныйПервый = false;
                ЛокальныйЗапуск();
            }

        }
        public void gui_ЛокальныйЗапуск()
        {
        }
        #endregion

        #region WORLD
        public task.chunk.world чанк
        {
            get
            {
                return manager.мир<task.chunk.world>(this);
            }
        }

        public task.Region.block.world блок
        {
            get
            {
                return manager.мир<task.Region.block.world>(this);
            }
        }
        public task.Region.cube.editor.world w_РедактируемыйКубик
        {
            get
            {
                return manager.мир<task.Region.cube.editor.world>(this);
            }
        }
        public task.Region.cube.editor.add.world w_ДобавлениеКубика
        {
            get
            {
                return manager.мир<task.Region.cube.editor.add.world>(this);
            }
        }
        public task.Region.world w_REGION
        {
            get
            {
                return manager.мир<task.Region.world>(this);
            }
        }

        public task.Region.cube.editor.world РедакторКубика
        {
            get
            {
                return manager.мир<task.Region.cube.editor.world>(this);
            }
        }
        #endregion
    }
}
