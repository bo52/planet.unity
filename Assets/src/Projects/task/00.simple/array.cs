﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
namespace simple
{
    using utility;
    public class array
    {
        static private array instance = new array();
        static public byte тип=0;

        public byte MAX = 1;
        public uint ВАРИАЦИЯ =2;
        public uint Координата(bool[] bs)
        {
            return bs.Координата(MAX);
        }

        public string structure(int count=-1)
        {
            switch (тип)
            {
                case 0:return "byte[" + (count<0?"":count.ToString()) + "]";
                case 1: return "Dictionary<byte, byte>";
            }
            return "";
        }
        uint CNT;

        virtual public bool ВнешниеВычисления(ref string Код, bool[] bs, byte i) {
            Код += i.ToString();
            return true;
        }
        virtual public string Кейс(string Код,byte всего)
        {
           return structure(всего)+ "{" + Код + "}";
        }
        virtual public void ДобавитьОднуСтруктуру(ref StreamWriter sw, bool[]bs) {
            if (Координата(bs) == 0)
                return;

            string Код = "";
            byte count = 0;

            for (byte i = 0; i < MAX; i++)
            {
                if (i>0)
                    Код += ",";
                if (!bs[i])
                {
                    Код += "0";
                    continue;
                }

                count++;
                ВнешниеВычисления(ref Код, bs, i);          
            }
            if (Код.Length == 0)
                return;
            CNT++;

            //Код = Координата(bs) + "," + Код;
            Код = "{" + Код + "},";
            sw.WriteLine(Код);
        }

        public uint МаксимальнаяКоордината()
        {
            uint test = 0;
            for (byte i = 0; i < MAX; i++)
            {
                test += test+(uint)Mathf.Pow(2, i);
            }
            return test;
        }

        public void Обход(ref StreamWriter sw, bool[] bs,int i)
        {
            i++;
            for (byte x = 0; x < 2; x++)
            {
                bs[i] = x == 1;
                if (i < MAX-1)
                {
                    Обход(ref sw, bs,i);
                }
                else
                {
                    ДобавитьОднуСтруктуру(ref sw,bs);
                }
            }
        }

        public void СформироватьМассивВФайл()
        {
            CNT = 0;
            bool[] bs = new bool[MAX];

            string s= structure();
            StreamWriter sw = new StreamWriter(Application.dataPath + "/test.ncs", false);
            sw.WriteLine("static public byte[,] МассивВозможностей=new byte[255,8]{");
            //sw.WriteLine("switch(ИД){");
            Обход(ref sw, bs, -1);
            sw.WriteLine("};");
            //sw.WriteLine("return null;");
            //sw.WriteLine("}");
            sw.Close();
        }
    }
}
