﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

using System;
namespace simple
{
    public class view
    {
        #region Экземпляр
        public string Имя = "empty";
        public bool showPosition = true;

        #region Выполнить
        /// <summary>
        /// выполнить тест абстрактного проекта
        /// </summary>
        virtual public void ВыполнитьТест() {}
        virtual public void Выполнить() { }
        #endregion

        public view()
        {
            ID.Add(this);
        }
        public void gui()
        {
            showPosition = EditorGUILayout.Foldout(showPosition, Имя);
            if (showPosition)
            {
                if (Selection.activeTransform)
                {
                    ВыполнитьТест();
                }
            }

            if (!Selection.activeTransform)
            {
                showPosition = false;
            }
        }
        #endregion

        /// <summary>
        /// ид массив проектов
        /// </summary>
        public static List<view> ID = new List<view> {};
        static public void example()
        {
            for (byte i = 0; i < ID.Count; i++)
            {
                ID[i].gui();
            }
        }
    }
}
