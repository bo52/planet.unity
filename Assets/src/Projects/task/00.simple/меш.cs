﻿using System.Collections.Generic;
using UnityEngine;

namespace simple
{
    using task.Region;
    using utility;
    public class меш: utility.меш
    {
        #region СОБРАТЬ
        public void СозданиеСвязейДляЧетырёхВершинСДвумяТреугольниками()
        {
            for (byte i = 0; i < 2; i++)
                ЗакрытьТреугольник((byte)(3 * i));
        }
        ///массив четырех вершин {порядок (индексы) создания двух треугольников для квадрата
        static public byte[] МассивСозданияКвадрата = new byte[6] { 0, 2, 3, 3, 1, 0 };
        public void ЗакрытьТреугольник(byte I)
        {
            for (byte i = 0; i < 3; i++)
            {
                    ts.Add(vs.Count + МассивСозданияКвадрата[I + i]);
            }
        }
        public Vector3 Вершина(Vector3 Центр, byte F,byte i,sbyte scale)
        {
            return Центр + Mathf.Pow(2, scale-1) * ВершиныКуба[ВершиныГранейКуба[F, i]];
        }

        /// <summary>
        /// развертка по типу куб по 6 направлениям и сохранение новой вершины куба в vertex
        /// </summary>
        /// <param name="F">грань куба из шести граней</param>
        /// <param name="v">вектор-вершина куба</param>
        /// <summary>
        /// v - новая вершина
        /// F - индекс поверхности от 0 до 5
        /// </summary>
        /// <param name="Вершина"></param>
        public void ДобавитьВершину(byte F, Vector3 v)
        {
                vs.Add(v);
                uvs.Add(F.uv(v));
        }

        public void СобираемКвадрат(Vector3 Центр,byte F, sbyte scale)
        {
            СозданиеСвязейДляЧетырёхВершинСДвумяТреугольниками();
            for (byte i = 0; i < 4; i++)
            {
                Vector3 v = Вершина(Центр,F, i, scale);
                ДобавитьВершину(F,v);
            }
        }
        public void Куб(Vector3 Центр, sbyte scale = 0, byte[] F = null)
        {
            if (F == null)
                F = new byte[] { 0, 1, 2, 3, 4, 5 };

            foreach (byte f in F)
            {
                СобираемКвадрат(Центр, f, scale);
            }
        }
        #endregion
    }
}
