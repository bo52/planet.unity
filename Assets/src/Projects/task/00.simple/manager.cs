﻿//файл хранит вспомогательные объекты из сцены в массиве по индексу и по именному свойству
using System.Collections.Generic;
using UnityEngine;

namespace simple
{
    public class manager
    {
        #region Экземпляры ООП
        static public task.chunk.cell.world ТестоваяЯчейка
        {
            get
            {
                return world_name("cell").GetComponent<task.chunk.cell.world>();
            }
        }
        static public GameObject ТестовыйЧанкНаСцене
        {
            get
            {
                return world_name("chunk");
            }
        }
        static public Chunk ТестовыйЧанк
        {
            get
            {
                return task.chunk.math_chunk_create.ПервыйЧанк(world_name("chunk"));
            }
        }
        #endregion
        #region WORLD
        static private GameObject fworld;
        public static T мир<T>(MonoBehaviour w)
        {
            return w.gameObject.GetComponent<T>();
        }

        static public GameObject world_name(string name)
        {
                return GameObject.Find("world_"+name);
        }

        static public GameObject world
        {
            get
            {
                if (fworld == null)
                {
                    fworld = GameObject.Find("world");
                    if (fworld == null)
                    {
                        fworld = new GameObject();
                        fworld.name = "world";
                    }
                }
                return fworld;
            }
        }
        #endregion

        static private GameObject fplayer;

        static public GameObject материал(string name)
        {
            GameObject go = Object.Instantiate(Resources.Load("materials/" + name, typeof(GameObject)), Vector3.zero, Quaternion.identity) as GameObject;
            go.name = name;
            return go;
        }
        static public GameObject editor
        {
            get
            {
                GameObject go = GameObject.FindGameObjectWithTag("editor");
                if (go == null)
                {
                    go = материал("editor");
                    go.tag = "editor";

                }
                return go;
            }
        }
        static public GameObject atom
        {
            get
            {
                GameObject go = GameObject.FindGameObjectWithTag("atom");
                if (go == null)
                {
                    go = материал("atom");
                    go.tag = "atom";

                }
                return go;
            }
        }

        static public GameObject player
        {
            get
            {
                return GameObject.Find("Player");
            }
        }
        //static public List<ObjectManager> ms = new List<ObjectManager>();
        public Dictionary<string, GameObject> objs = new Dictionary<string, GameObject>();


        public GameObject ПолучитьОбъект(string name)
        {
            if (objs.ContainsKey(name))
            {
                return objs[name];
            }
            GameObject obj = GameObject.Find(name);
            //объекта нет на сцене
            //if (obj==null)

            objs.Add(name, obj);
            return obj;
        }

    }
}
