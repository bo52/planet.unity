﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using utility;
using System.Linq;


public class Root
{
    static public Root instance = new Root(Vector3.zero,1,true);

    public Root Parent=null;
    public bool Активный = true;
    public Root[] RS = new Root[8];
    public sbyte scale = 0;
    public Vector3 Центр = Vector3.zero;

    static Dictionary<uint, List<Mesh>> tms = new Dictionary<uint, List<Mesh>>();
    Root(Vector3 Центр, int scale = 1,bool Активный=false,Root Parent=null)
    {
        this.Parent = Parent;
        this.Активный = Активный;
        this.Центр = Центр;
        this.scale = (sbyte)scale;
    }

    public void Построить()
    {
        tms.Clear();
        //Построен.Clear();
        //foreach (var i in new byte[] { 0, 1, 4, 5 })
            //RS[i].Math_LOD();
    }

    public void test()
    {
        VertexGlobal.CELLS.Clear();
        //ПоУмолчанию();

        if (Активный)
        {
            foreach (var i in new byte[] {0,1,4,5})
            {
                //RS[i] = new Root(Центр + 0.5f * R * math_cell_triangle.Вершины[i], scale - 1,false,this);
                //RS[i].ПоУмолчанию(false);
            }
        }
        Перестроить();
    }

    #region СБОРКА

    #endregion

    public void Перестроить()
    {
        tms.Clear();
        //Построен.Clear();
        VertexGlobal.temp.Clear();

        if (!Активный)
        {
            this.СобратьЯчейкиЧанка();
            return;
        }
        foreach (var R in RS)
        {
            if (R == null) continue;
            R.СобратьЯчейкиЧанка();
        }
    }

}
