using UnityEngine;
using utility;
using utility.go;
namespace task.test.texture
{
    public class world : simple.world
    {
        //������������
        static public Vector3 v0 = Vector3.zero;
        static public Vector3 v1 = Vector3.forward;
        static public Vector3 v2 = Vector3.right;

        static public Vector2 uv0 = new Vector2(0, 1);
        static public Vector2 uv1 = new Vector2(1, 1);
        static public Vector2 uv2 = new Vector2(1, 0);
        static public byte scale = 2;
        public float part = Mathf.Pow(scale, -1);

        public override void gui()
        {
            GUILayout.Label("part=" + part);
            if (v0.������("0") || v1.������("1") || v2.������("2") || uv0.������("uv0") || uv1.������("uv1") || uv2.������("uv2"))
            {
                ��������������();
            }
            //(0, 1)=(0.0f,1.0f)=(0.0f,0.5f)=(0.5f,1.0f)
            //(1, 1)=(0.5f,1.0f)=(0.0f,1.0f)=(0.5f,0.5f)
            //(1, 0)=(0.0f,0.5f)=(0.5f,1.0f)=(0.0f,0.5f)

            //7 + x
            //(0.5f,1.5f)
            //(1.0f,1.5f)
            //(0.5f,1.0f)
            //3 - x
            //(0.0f,1.5f)
            //(0.5f,1.5f)
            //(0.0f,1.0f)
            //5 - y
            //(0.5f,1.0f)
            //(1.0f,1.0f)
            //(0.5f,0.5f)
            //1
            //(0.0f,1.0f)
            //(0.5f,1.0f)
            //(0.0f,0.5f)

            if (utility.GUI.������("��������"))
            {
                v0 = Vector3.zero;
                v1 = Vector3.forward;
                v2 = Vector3.right;
                uv0 = new Vector2(0.0f, 1.0f);
                uv1 = new Vector2(0.5f, 1.0f);
                uv2 = new Vector2(0.0f, 0.5f);
                ��������������();
            }
            if (utility.GUI.������("��������"))
            {
                v0 = Vector3.zero;
                v1 = Vector3.forward;
                v2 = Vector3.right;
                uv0 = new Vector2(0.0f, 0.5f);
                uv1 = new Vector2(0.0f, 1.0f);
                uv2 = new Vector2(0.5f, 1.0f);
                ��������������();
            }
        }

        Vector2[] vs2 = new Vector2[]{
            new Vector2(0, 0),new Vector2(1, 0),new Vector2(0, 1),new Vector2(1, 1)
        };

        public void ��������������������(int z0=0)
        {
            for (var x = 0; x < 2; x++)
            {
                var v = new Vector3(x, 0, z0);
                var _uv0 = uv0;
                var _uv1 = uv1;
                var _uv2 = uv2;

                var uv = part * new Vector2(v.x, v.z);


                _uv0 = uv + part * new Vector2(0, 0);
                _uv1 = uv + part * new Vector2(1, 1);
                _uv2 = uv + part * new Vector2(1, 0);
                (v0 + v).triangle(v1 + v2 + v, v2 + v, _uv0, _uv1, _uv2).��������������������(gameObject.transform, 0, 1, 1);

                _uv0 = uv + part * new Vector2(0, 1);
                _uv1 = uv + part * new Vector2(1, 1);
                _uv2 = uv + part * new Vector2(0, 0);
                (v1 + v).triangle(v1 + v2 + v, v0 + v, _uv0, _uv1, _uv2).��������������������(gameObject.transform, 0, 1, 1);
            }
        }

        public void �����������(int z0 = 1)
        {
                for (var x = 0; x < 2; x++)
                {
                    var v = new Vector3(x, 0, z0);
                    var _uv0 = uv0;
                    var _uv1 = uv1;
                    var _uv2 = uv2;

                    var uv = part * new Vector2(x, z0);

                    _uv0 = uv + part * new Vector2(0, 0);
                    _uv1 = uv + part * new Vector2(0, 1);
                    _uv2 = uv + part * new Vector2(1, 0);
                    (v0 + v).triangle(v1 + v, v2 + v, _uv0, _uv1, _uv2).��������������������(gameObject.transform, 0, 1, 1);

                    _uv0 = uv + part * new Vector2(1, 1);
                    _uv1 = uv + part * new Vector2(1, 0);
                    _uv2 = uv + part * new Vector2(0, 1);
                    (v1 + v2 + v).triangle(v2 + v, v1 + v, _uv0, _uv1, _uv2).��������������������(gameObject.transform, 0, 1, 1);
                }
        }

        public override void ��������������()
        {        
            gameObject.transform.����������������();

            �����������();
            ��������������������();
        }
        //�������� ������ empty
        //������ ����
        void Start()
        {          
        }
    }
}
