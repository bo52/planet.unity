﻿using UnityEngine;
using UnityEditor;
namespace task.Player
{
    [CustomEditor(typeof(world))]
    public class gui : simple.gui
    {
        //при клике привязанного игрового объекта
        public gui(){
        }

        public override void OnInspectorGUI()
        {
            GUILayout.TextField("Пройденное Расстояние=" + world.ПройденноеРасстояние.ToString());
            GUILayout.TextField("Текущий Регион=" + Player.moving.world.ТекущийРегион_Вектор.ToString());
        }
        public override void OnSceneGUI()
        {            
        }
    }
}
