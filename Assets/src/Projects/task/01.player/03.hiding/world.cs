﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace task.Player.hiding
{
    using utility;
    public class world : MonoBehaviour
    {
        static public bool ВсеБлокиВидимы = false;
        static public float СмещениеКамеры = 1.00f;
        static public Vector3 ТекущаяЯчейкаПространства = Vector3.zero;
        static public bool СкрытиеРяда = false;   
        
        // Start is called before the first frame update
        void Start()
        {           
            ТекущаяЯчейкаПространства = manager.player.transform.position.Целое();

        }

        // Update is called once per frame
        void Update()
        {
            this.ПроверкаНаСкрытиеРяда();
        }
    }
}
