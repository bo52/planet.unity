﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace task.Player.hiding
{
    using utility;
    static public class math
    {
        static Vector3[,] _vs = new Vector3[,]{
            {Vector3.left, Vector3.zero, Vector3.right },
            {Vector3.back, Vector3.zero, Vector3.forward },
            {Vector3.left, Vector3.zero, Vector3.right },
            {Vector3.back, Vector3.zero, Vector3.forward },
        };

    static public bool ВЭтомРегионеПоказатьПоверхность(this Region.block.world R)
        {
            for (byte i=0;i<3;i++)
            {
                if (R.Координата == moving.world.ТекущийРегион_Вектор + _vs[rotation.world.pov, i])
                    return true;
            }
            return false;
        }

        static public void ПоказатьПоверхность(this Region.block.world R, Region.block.меш m,Vector3 dv)
        {           
            if (world.ВсеБлокиВидимы) return;
            if (!R.ВЭтомРегионеПоказатьПоверхность()) return;

            byte f = array.fs[rotation.world.pov];
            Vector3 v = moving.world.ТекущаяЯчейкаПространстваЛокально;
            bool b = false;
            switch (rotation.world.pov)
            {
                case 0:
                    b = dv.z == v.z - world.СмещениеКамеры;
                    break;
                case 1:
                    b = dv.x == v.x + world.СмещениеКамеры;
                    break;
                case 2:
                    b = dv.z == v.z + world.СмещениеКамеры;
                    break;
                case 3:
                    b = dv.x == v.x - world.СмещениеКамеры;
                    break;
            }
            if (b)
            {
                //Region.block.world.РегионИзменился(ref dv, ref R, v);
                m.СобираемКвадрат(dv, f, 0);
            }
        }



        static public void ПроверкаНаСкрытиеРяда(this world p)
        {
            Vector3 новый = p.transform.position.Целое();
            if (новый == world.ТекущаяЯчейкаПространства) return;
            switch (rotation.world.pov)
            {
                case 0:
                    if (новый.z == world.ТекущаяЯчейкаПространства.z) return;
                    break;
                case 1:
                    if (новый.x == world.ТекущаяЯчейкаПространства.x) return;
                    break;
                case 2:
                    if (новый.z == world.ТекущаяЯчейкаПространства.z) return;
                    break;
                case 3:
                    if (новый.x == world.ТекущаяЯчейкаПространства.x) return;
                    break;
                default:
                    return;
            }
            world.ТекущаяЯчейкаПространства = новый;
            world.СкрытиеРяда = true;
        }

        static public bool СкрытыйБлок(this Region.block.world R, Vector3 КоординатаКубика)
        {
            if (world.ВсеБлокиВидимы) return false;
            Vector3 v = R.ГлобальныйВекторКубика(КоординатаКубика);

            switch (rotation.world.pov)
            {
                case 0:
                    return v.z < world.ТекущаяЯчейкаПространства.z - world.СмещениеКамеры;
                case 1:
                    return v.x > world.ТекущаяЯчейкаПространства.x + world.СмещениеКамеры;
                case 2:
                    return v.z > world.ТекущаяЯчейкаПространства.z + world.СмещениеКамеры;
                case 3:
                    return v.x < world.ТекущаяЯчейкаПространства.x - world.СмещениеКамеры;
                default:
                    return false;
            }
        }
    }
}
