﻿using UnityEngine;

namespace task.Player
{
    static class math
    {

        static public void ПроверкаНаРендеринг(this world p)
        {
            if (world.НетДвижения) return;

            if (hiding.world.СкрытиеРяда || rotation.world.НачатПоворот || world.ОбновитьРегионы)
            {
                hiding.world.СкрытиеРяда = false;
                world.ОбновитьРегионы = false;

                world.НетДвижения = true;

                if (rotation.world.НачатПоворот)
                    hiding.world.ВсеБлокиВидимы = true;

                p.transform.position.Рендеринг();

                hiding.world.ВсеБлокиВидимы = false;
                rotation.world.НачатПоворот = false;
            }
        }
        static public void Рендеринг(this Vector3 player)
        {

            Region.math_region_far.УдалитьДальниеРегионы(player);
            Region.math_region_load.ЗагрузкаРегионов();
            world.НетДвижения = false;
        }
    }
}
