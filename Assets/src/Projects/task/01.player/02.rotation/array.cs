﻿using UnityEngine;

namespace task.Player.rotation
{
    static public class array
    {
        static public readonly string[] КнопкиПоворота = new string[2] { "e", "q" };
        static public readonly sbyte[] НаправлениеПоворота = new sbyte[2] { -1, +1 };
        //направление 0-3 Восток - Север - Запад - Юг
        static public readonly KeyCode[,] КнопкиДвижений = new KeyCode[4, 4] {
            { KeyCode.A, KeyCode.D, KeyCode.W, KeyCode.S },
            { KeyCode.W, KeyCode.S, KeyCode.D, KeyCode.A },
            { KeyCode.D, KeyCode.A, KeyCode.S, KeyCode.W },
            { KeyCode.S, KeyCode.W, KeyCode.A, KeyCode.D }
        };
}
}
