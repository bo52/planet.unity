﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace task.Player.rotation
{
    static public class math
    {
        static public void ОбновитьКнопкиДвижений(this world p)
        {
            if (p == null) return;
            for (byte i = 0; i < 4; i++)
                p.GetComponent<moving.world>().КнопкиДвижений[i] = array.КнопкиДвижений[world.pov, i];
        }
        static bool НастроитьДвижениеПослеПоворота(this world p, byte i)
        {
            if (!Input.GetKeyDown(array.КнопкиПоворота[i]))
                return false;

            p.b_root = array.НаправлениеПоворота[i];
            p.rot = 0;
            world.pov -= p.b_root;
            if (world.pov < 0) world.pov = 3;
            if (world.pov > 3) world.pov = 0;

            p.ОбновитьКнопкиДвижений();
            world.НачатПоворот = true;
            return true;
        }

        static public void НачатьПоворот(this world p)
        {
            if (p.rot > -1) return;

            for (byte i = 0; i < array.КнопкиПоворота.Length; i++)
            {
                if (p.НастроитьДвижениеПослеПоворота(i))
                    return;
            }
        }

        static public void ПроцессПоворота(this world p)
        {
            if (p.rot < 0) return;

            p.transform.rotation *= Quaternion.AngleAxis(2 * p.b_root, new Vector3(0, 1, 0));
            p.rot += 1;

            if (p.rot >= 45)
            {
                p.rot = -1;
                Player.math.Рендеринг(p.transform.position);
            }
        }
    }
}
