﻿using UnityEngine;

namespace task.Player.moving
{
    public class world : MonoBehaviour
    {
        public KeyCode[] КнопкиДвижений = new KeyCode[4] {
            KeyCode.A,
            KeyCode.D,
            KeyCode.W,
            KeyCode.S
        };
        static public bool ИзменилсяРегион = false;
        static public ulong ТекущийРегион = 0;
        static public Vector3 ТекущийРегион_Вектор = Vector3.zero;
        static public Vector3 ТекущаяЯчейкаПространстваЛокально = Vector3.zero;

        public void Start()
        {
            this.ОбновитьТекущуюКоординатуРегиона();
        }

        // Update is called once per frame
        public virtual void Update()
        {           
            this.НовоеПоложениеИгрока();
            this.ОбновитьТекущуюКоординатуРегиона();
        }
    }
}
