﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace task.Player.moving
{
    using utility;
    static public class math
    {
        static readonly public Vector3[] Направление = new Vector3[4]{
            Vector3.left,
            Vector3.right,
            Vector3.forward,
            Vector3.back
        };

        static public void ОбновитьЛокально(this world p)
        {
            Vector3 v = p.transform.position.Целое();
            v = Region.block.math.D * world.ТекущийРегион_Вектор.ABS() - v.ABS();
            world.ТекущаяЯчейкаПространстваЛокально = v.ABS();
        }

        static public void ОбновитьТекущуюКоординатуРегиона(this world p)
        {
            Vector3 v= p.transform.position.КоординатаРегионаОтГлобальной();

            world.ИзменилсяРегион = world.ТекущийРегион_Вектор!=v;
            world.ТекущийРегион_Вектор = v;
            world.ТекущийРегион = Region.world.КоординатаРегиона(world.ТекущийРегион_Вектор);
            p.ОбновитьЛокально();

        }
        static public sbyte ИндексНажатияКнопки(this world p)
        {
            for (sbyte i = 0; i < 4; i++)
                if (Input.GetKey(p.КнопкиДвижений[i]))
                    return i;
            return -1;
        }
        static public void НовоеПоложениеИгрока(this world p)
        {
            sbyte key = p.ИндексНажатияКнопки();
            if (key < 0)
                return;
            p.transform.position = Vector3.MoveTowards(p.transform.position, p.transform.position + Направление[key], Time.deltaTime);
        }
    }
}
