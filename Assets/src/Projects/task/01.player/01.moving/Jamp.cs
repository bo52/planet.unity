﻿using UnityEngine;

namespace task.Player.moving
{
    public class Jamp : MonoBehaviour
    {
        public bool НаЗемле = true;
        public byte СилаПрыжка = 30;
        public float distance = 2.0f;//настроить максимальное расстояние падения луча в зависимости от объекта (от которого падает луч)

        void Update()
        {
            прыжок();
        }

        public void прыжок()
        {
            Ray ray = new Ray(transform.position, Vector3.down);
            RaycastHit rh;

            НаЗемле = Physics.Raycast(ray, out rh, distance);

            if (Input.GetAxis("Jump") != 0)
            {
                if (НаЗемле)
                    GetComponent<Rigidbody>().AddForce(Vector3.up * СилаПрыжка);
            }
        }
    }
}
