﻿using UnityEngine;
namespace task.Player
{
    public class world : MonoBehaviour
    {
        static public bool НетДвижения = false;
        static public bool ОбновитьРегионы = false;
        static public float ПройденноеРасстояние = 0;
        static public Vector3 ПредыдущееПоложение = Vector3.zero;
        //тестовый проект квадрат
        //запуск игры
        void Start()
        {
        }
        void Update()
        {
            ПройденноеРасстояние = Vector3.Distance(ПредыдущееПоложение, transform.position);
            if (ПройденноеРасстояние > 16)
            {
                ПредыдущееПоложение = transform.position;
                ОбновитьРегионы = true;
            }
            this.ПроверкаНаРендеринг();
        }
    }
}
