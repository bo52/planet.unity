﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using utility;
namespace task.Region.cube
{
    static public class math_fs
    {
        static Vector3[] Сосед = new Vector3[] { Vector3.left, Vector3.right, Vector3.down, Vector3.up, Vector3.back, Vector3.forward };

        static public bool add_СоседнийКубикМодифицировался(this List<byte> FS,int index)
        {
            if (index == -1) return false;
            FS.RemoveAt(index);
            return true;
        }
        static public bool del_СоседнийКубикМодифицировался(this List<byte> FS, int index,byte F)
        {
            if (index != -1) return false;
            FS.Add(F);
            return true;
        }

        static public bool МодификацияГраниСоседа(this block.world R, int i,byte f,bool add)
        {
            byte fs = (byte)R.КубикиРегиона[i][1];
            List<byte> FS = fs == 0?new List<byte>(): fs.SQA().ToList();

            byte F = f.ИнверсияГрани();
            int index=FS.FindIndex(x => x == F);

            if (add ? FS.add_СоседнийКубикМодифицировался(index) : FS.del_СоседнийКубикМодифицировался(index, F))
            {
                fs = FS.МассивГранейВЧисло();
                if (fs == 0)
                    R.КубикиРегиона.RemoveAt(i);
                else
                    R.КубикиРегиона[i][1] = fs;
            }

            return true;
        }

        static public bool МодификацияРегиона(this bool[] bs, block.world R,Vector3 dv, byte f,bool add)
        {
            int index = R.ЕстьКубик(dv);
            if (index == -1)
            {
                bs[f] = true;
                return false;
            }

            return R.МодификацияГраниСоседа(index, f,add);
        }

        static public bool МодификацияСоседнегоРегиона(this bool[] bs, block.world W, Vector3 dv,byte f, bool add)
        {
            if (math_region_next.ВходитВРегион(dv)) return false;

            block.world w = math_region_load.ЗагружаемыйРегион(W.Координата + Сосед[f]);
            math_region_next.СледующийРегион(ref dv);

            if (bs.МодификацияРегиона(w, dv, f, add))
            {
                w.w_REGION.ОбновитьРегион();
                w.w_REGION.Сохранить();
            }

            return true;
        }

        static public void ИзменитьМассивГранейСоседнегоКуба(this bool[]bs, Vector3 v, block.world R,byte f,bool add)
        {
            bs[f] = false;
            Vector3 dv = v + Сосед[f];

            if (bs.МодификацияСоседнегоРегиона(R, dv, f, add)) return;
            bs.МодификацияРегиона(R, dv, f,add);
        }
        public static uint ИзменитьПоверхностиСоседей(this Vector3 v,bool add)
        {
            Region.editor.world R = Region.editor.world.РедактируемыйРегион;
            bool[] bs = new bool[6];

            for (byte f = 0; f < 6; f++)
                bs.ИзменитьМассивГранейСоседнегоКуба(v, R.блок, f,add);
            return bs.Координата(6);
        }
    }
}