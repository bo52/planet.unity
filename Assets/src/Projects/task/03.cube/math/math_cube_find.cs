﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using utility.go;
namespace task.Region.cube
{
    using utility;
    static public class math_cube_find
    {
        static public int ЕстьКубик(this Vector3 dv, ref GameObject gw,ref Vector3 v)
        {
            math_region_next.СледующийРегион(ref v, ref gw, dv);
            if (gw == null) return 0;
            return gw.w_REGION().блок.ЕстьКубик(v);
        }

    }
}
