﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using utility;
namespace task.Region.cube.editor.add
{
    static public class math_f_add
    {
        static public bool ДобавлениеПоверхности(ref List<byte> fs, byte F)
        {
            if (ЕстьПоверхность(fs, F) != -1) return false;
            fs.Add(F);
            return true;
        }
        static public int ЕстьПоверхность(List<byte> fs, byte F)
        {
            return fs.FindIndex(x => x == F);
        }

        static public void ДобавлениеПоверхности(this world w, int i, byte F)
        {
            List<byte> Fs = w.w_РедактируемыйКубик.FS(i);
            if (add.math_f_add.ДобавлениеПоверхности(ref Fs, F))
                w.w_РедактируемыйКубик.set_fs(i, Fs);
        }
    }
}
