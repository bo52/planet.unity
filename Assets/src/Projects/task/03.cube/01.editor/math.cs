using UnityEngine;

namespace task.Region.cube.editor
{
    static public class math
    {
        static public Vector3 normal
        {
            get
            {
                return hit.world.HIT.normal;
            }
        }
        static public Vector3 localScale
        {
            get
            {
                float k = 0.5f;
                if (normal == Vector3.back) return new Vector3(1, 1, k);
                if (normal == Vector3.forward) return new Vector3(1, 1, k);
                if (normal == Vector3.down) return new Vector3(1, k, 1);
                if (normal == Vector3.up) return new Vector3(1, k, 1);
                if (normal == Vector3.right) return new Vector3(k, 1, 1);
                if (normal == Vector3.left) return new Vector3(k, 1, 1);
                return Vector3.one;
            }
        }
        static public string[] size_options = new string[] { "0", "1", "2", "4", "8", "16" };
        static public byte SIZE = 1;//������ �������������� ������ 
        static public int get_size_int
        {
            get
            {
                return System.Convert.ToInt32(size_options[SIZE]);
            }
        }
        static public float get_size
        {
            get
            {
                return 1 / (float)get_size_int;
            }
        }
        static public Vector3 localSclale_size
        {
            get
            {
                return localScale * get_size;
            }
        }

        static public Vector3 �������������
        {
            get
            {
                if (Region.editor.world.������������������� == null)
                    return Vector3.zero;

                Vector3 v = hit.world.HIT.point - Region.editor.world.�������������������.transform.position - world.������������;
                //v *= 2;
                v += 0.5f * Vector3.one;
                //v = v.���������������();
                return v;
            }
        }
        static public Vector3 calc_atom_zero
        {
            get
            {
                float k = 0;
                for (int i = 0; i < SIZE - 1; i++)
                {
                    k += 0.25f * 1 / Mathf.Pow(2, i);
                }

                return -Vector3.one * k;
            }
        }
        static public Vector3 calc_atom
        {
            get
            {
                float pow = Mathf.Pow(2, SIZE);
                float part = 1 / pow;
                Vector3 v = Region.cube.editor.manager.atom.transform.localPosition - part * normal;
                Vector3 v0 = calc_atom_zero + world.������������;
                v -= v0;
                v *= 0.5f * pow;
                return v;
            }
        }

        static public void ��������(this Transform atom, Transform R)
        {
            atom.SetParent(R);
            atom.position = R.position + world.������������ + 0.5f * normal;
            atom.localScale = localSclale_size;
            atom.position += 0.005f * normal;

            Vector3 v = normal;
            v = new Vector3(Mathf.Abs(v.x), Mathf.Abs(v.y), Mathf.Abs(v.z));
            v -= Vector3.one;
            float part = 0.25f;
            for (int i = SIZE; i > 1; i--)
            {
                atom.transform.position += part * v;
                part *= 0.5f;
            }
            Vector3 v1 = ������������� * Mathf.Pow(2, SIZE - 1);
            v1.x = v.x == 0 ? 0 : (int)v1.x;
            v1.y = v.y == 0 ? 0 : (int)v1.y;
            v1.z = v.z == 0 ? 0 : (int)v1.z;
            v1 /= Mathf.Pow(2, SIZE - 1);

            atom.transform.position += v1;
            v = atom.transform.position;
            if (normal.x != 0) atom.transform.position = new Vector3(hit.world.HIT.point.x, v.y, v.z);
            if (normal.y != 0) atom.transform.position = new Vector3(v.x, hit.world.HIT.point.y, v.z);
            if (normal.z != 0) atom.transform.position = new Vector3(v.x, v.y, hit.world.HIT.point.z);
            world.��������������� = calc_atom;
        }
        static public bool ���������������������(ref Vector3 v)
        {
            float pow = Mathf.Pow(2, SIZE - 1);
            bool b = false;
            if (v.x < 0)
            {
                v.x = pow - 1;
                b = true;
            }
            if (v.y < 0)
            {
                v.y = pow - 1;
                b = true;
            }
            if (v.z < 0)
            {
                v.z = pow - 1;
                b = true;
            }
            if (v.x >= pow)
            {
                v.x = 0;
                b = true;
            }
            if (v.y >= pow)
            {
                v.y = 0;
                b = true;
            }
            if (v.z >= pow)
            {
                v.z = 0;
                b = true;
            }
            return b;
        }
    }
}
