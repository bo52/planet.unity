﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Vertex
{
    public Vector3 глВектор;
    public bool активный = false;
    public Vector3 dv = Vector3.zero;
    static public readonly byte[] ВторыеВершиныРебра = new byte[] { 1, 3, 2, 3, 4, 6, 5, 7, 5, 7, 6, 7, };
    static public readonly byte[] ГлавныеВершиныРебра = new byte[] { 0, 1, 0, 1, 0, 2, 1, 3, 4, 6, 4, 5, };
    public Vector3[] Рёбра = new Vector3[3];

    Vector3 Соcедний(int lod_x, int lod_y, int lod_z, Vector3 v, Math_LOD lod,Vector3 vi)
    {
        var vertex = lod.СозданиеВершины(lod_x, lod_y, lod_z, v + lod.dx * vi);
        if (vertex.активный)
            dv = vertex.dv;
        return vertex.глВектор - 0.5f * vi + dv;
    }

    public Vertex(int lod_x, int lod_y, int lod_z, Vector3 v,Math_LOD lod)
    {
        глВектор = this.ВычислениеАктивнойВершины(v, ref lod);
        Рёбра[0] = глВектор + 0.5f * Vector3.right - dv;
        Рёбра[1] = глВектор + 0.5f * Vector3.up - dv;
        Рёбра[2] = глВектор + 0.5f * Vector3.forward - dv;
        if (активный) return;

        if (lod_x + 1 < lod.lod.GetLength(0)) Рёбра[0] = Соcедний(lod_x + 1, lod_y, lod_z, v, lod, Vector3.right);
        if (lod_y + 1 < lod.lod.GetLength(0)) Рёбра[1] = Соcедний(lod_x, lod_y + 1, lod_z, v, lod, Vector3.up);
        if (lod_z + 1 < lod.lod.GetLength(0)) Рёбра[2] = Соcедний(lod_x, lod_y, lod_z + 1, v, lod, Vector3.forward);
    }

    Vector3 ВекторРебра(Math_LOD lod,Vector3 def)
    {
        Vector3 выч = def;
        if (!активный)
        {
            Vector3 текВершинаЯч = Vector3.zero;
            for (int x = -lod.dx_2; x <= lod.dx_2; x++)
                for (int y = -lod.dx_2; y <= lod.dx_2; y++)
                    for (int z = -lod.dx_2; z <= lod.dx_2; z++)
                    {
                        if (x == 0 && y == 0 && z == 0)
                            continue;
                        //ЗАНЯТ ЛИ?
                        текВершинаЯч = def + new Vector3(x, y, z);
                        if (lod.busy.FindIndex(t => t == текВершинаЯч) != -1)
                            continue;

                        выч = текВершинаЯч;
                        активный = lod.ch.ЕстьТочка(выч);
                        if (активный)
                            break;
                    }
        }
        lod.busy.Add(выч);
        return выч;
        //if (глВектор!= def)
        //глВектор -= 0.5f* Vector3.one;
    }
}
