﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

static public class math_cell_LOD
{
    enum LOD_opt_i:byte
    {
        шаг,//0
        part,//1
        cnt,//2
        x,//3
        y,//4
        z,//5
        код,//6
    }

    enum LOD_opt_v_i : byte
    {
        v0,//0 = текущая рассматриваемая ячейка
        Центр_dv,//1 = Смещение Центра
        dup, //2 = УменьшитьРебро
        Вершина,//3 = Рассматриваемая Вершина с шагом (растянутая)
        LOD_Вершина,//4 = Рассматриваемая Вершина без шага
        val,//5 = Добавляемый смещённый вектор от вершин
        dv,//6 = Смещение вершины к другой вершине
        СмещённаяВершинаКЦентруСШагом,//7 = индексированная вершина внутри рассматриваемой ячейки
    }


    static public Vector3 БлижайшаяАктивнаяВершина_LOD(this Chunk ch, Vector3[,,] LOD, Vector3 edge, Dictionary<byte, Vector3> ИндексыВершин, Vector3 v0, byte шаг)
    {
        //ближайший индекс вершины к ребру
        var i = (from x in ИндексыВершин.Keys.ToList() orderby Vector3.Distance(edge, math_cell_triangle.Вершины[x]) select x).First();
        //var ВекторВершины = v0 + шаг * math_cell.вершины[i];
        //получить смещения вершины
        //var offset = LOD[,,];
        //расстояние между ребром и вершиной определяет направление к вершине
        //var v = edge - math_cell.Вершины[i];
        return math_cell_triangle.Вершины[i] - Vector3.Scale(ИндексыВершин[i], math_cell_triangle.Вершины[i]);
    }

}
