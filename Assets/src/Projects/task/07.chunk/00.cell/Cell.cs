﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cell
{
    Vector3[] Соседи = new Vector3[] {
            Vector3.left,
            Vector3.right,
            Vector3.down,
            Vector3.up,
            Vector3.back,
            Vector3.forward,

            Vector3.left+Vector3.down,
            Vector3.left+Vector3.up,
            Vector3.right+Vector3.down,
            Vector3.right+Vector3.up,
            Vector3.back+Vector3.down,
            Vector3.back+Vector3.up,
            Vector3.forward+Vector3.down,
            Vector3.forward+Vector3.up,

            Vector3.back+Vector3.left+Vector3.down,
            Vector3.back+Vector3.left,
            Vector3.back+Vector3.left+Vector3.up,
            Vector3.forward+Vector3.left+Vector3.down,
            Vector3.forward+Vector3.left,
            Vector3.forward+Vector3.left+Vector3.up,

            Vector3.back+Vector3.right+Vector3.down,
            Vector3.back+Vector3.right,
            Vector3.back+Vector3.right+Vector3.up,
            Vector3.forward+Vector3.right+Vector3.down,
            Vector3.forward+Vector3.right,
            Vector3.forward+Vector3.right+Vector3.up,
    };

}
