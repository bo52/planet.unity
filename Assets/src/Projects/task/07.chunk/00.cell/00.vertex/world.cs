using UnityEngine;
using UnityEditor;
using utility;
using System.Collections.Generic;

namespace task.cell.vertex
{   
    public class world : simple.world
    {
        bool ��������=false;
        public override void ��������������()
        {
            Debug.Log("22");
            math_cell_triangle.����();
        }

        static public void ����������������������()
        {
            RaycastHit HIT;
            Ray ray = HandleUtility.GUIPointToWorldRay(Event.current.mousePosition);
            int layerMask = 1 << 9;
            if (!Physics.Raycast(ray, out HIT, Mathf.Infinity, layerMask)) return;
            if (HIT.collider == null) return;

            if (HIT.collider.gameObject.tag == "vertex")
                Selection.objects = new Object[1] { HIT.collider.gameObject };
        }

        public override void gui()
        {
            manager.������������.�������������������();
            manager.��������������.gui_move();
        }

        static public Dictionary<uint, Vector3[]> ��������������
        {
            get
            {
                return manager.������������.cells;
            }
        }
        static public void ����������������������(bool b,Vector3 v)
        {
            Chunk ch = manager.������������;
            uint id = Chunk.����������(v);
            if (b)
            {
                if (!ch.cells.ContainsKey(id)) ch.cells.Add(id,Chunk.new_offset);
            } else
            {
                if (ch.cells.ContainsKey(id)) ch.cells.Remove(id);
            }
            ch.�������();
        }

        public void gui_�������������������������()
        {
            if (Event.current.keyCode == KeyCode.B)
            {
                var ������������� = System.Convert.ToByte(gameObject.name);
                var v = gameObject.transform.parent.position + math_cell_triangle.�������[�������������];
                var cells = ��������������;
                if (Chunk.���������(v)) return;
                �����������������������(!��������);

                ����������������������(��������, v);

            }
        }

        public void �����������������������(bool ��������)
        {
            �������� = ��������;
            Renderer r = gameObject.GetComponent<Renderer>();
            string name = �������� ? "white" : "black";
            r.material = Resources.Load("materials/"+ name, typeof(Material)) as Material;
        }
        static public world[] ����������
        {
            get
            {
                world[] ws = new world[8];
                foreach (Transform child in task.chunk.cell.manager.world_name("cell").transform)
                {
                    ws[System.Convert.ToByte(child.name)] = child.GetComponent<world>();
                }
                return ws;
            }
        }

        static public bool �������������(Vector3 v)
        {
            var x = (int)v.x;
            var y = (int)v.y;
            var z = (int)v.z;
            if (x >= Chunk.SIZE || x < 0) return false;
            if (y >= Chunk.SIZE || y < 0) return false;
            if (z >= Chunk.SIZE || z < 0) return false;
            return true;
        }

        static public void ����������������������()
        {
            var cells= ��������������;
            int cnt = -1;
            bool b;
            int x;
            int y;
            int z;
            world[] ws=����������;
            Vector3 pos = manager.��������������.transform.position;

            foreach (var v in math_cell_triangle.�������)
            {
                cnt++;
                x = (int)(pos.x + v.x);
                y = (int)(pos.y + v.y);
                z = (int)(pos.z + v.z);

                b = �������������(new Vector3 (x, y, z));
                if (b) b = cells.ContainsKey(Chunk.����������(new Vector3(x, y, z)));

                ws[cnt].�����������������������(b);
            }
        }


        //�������� ������ empty
        //������ ����
        void Start()
        {          
        }

        private void Update()
        {
        }
    }
}
