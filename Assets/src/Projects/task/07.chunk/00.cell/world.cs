using UnityEngine;

namespace task.chunk.cell
{

    public class world : simple.world
    {
        Model.world ������;
        Cell A;

        public override void gui()
        {
            gui_move();
        }

        public void �����������������(Vector3 v)
        {
            gameObject.transform.position = v;
            task.cell.vertex.world.����������������������();
        }

        public void gui_move_btn(Vector3 dv,string name)
        {
            chunk.manager.world_name("chunk").����������();
            bool b = false;
            byte width = 80;
            var v = gameObject.transform.position;           
            switch (name)
            {
                case "left":
                    width = 79;
                    b = (v + dv).x >= -1;
                    break;
                case "right":
                    width = 79;
                    b = (v + dv).x < Chunk.SIZE;
                    break;
                case "down":
                    b = (v + dv).y >= -1;
                    break;
                case "up":
                    width = 79;
                    b = (v + dv).y < Chunk.SIZE;
                    break;
                case "-":
                    width = 40;
                    b = (v + dv).z >= -1;
                    break;
                case "+":
                    width = 40;
                    b = (v + dv).z < Chunk.SIZE;
                    break;
            }

            if (b)
            {
                if (utility.GUI.������(name, width))
                {
                    gameObject.transform.position += dv;
                    task.cell.vertex.world.����������������������();
                }
            } else
            {
                GUILayout.Space(width);
            }
        }

        public void ����������������()
        {
            gameObject.transform.position = Vector3.zero;
            task.cell.vertex.world.����������������������();
        }

        public void gui_move()
        {
            var v=gameObject.transform.position;
            GUILayout.Label((int)v.x+","+ (int)v.y + "," + (int)v.z);
            GUILayout.BeginVertical();

            GUILayout.BeginHorizontal();
            if (utility.GUI.������("zero", 79))
                ����������������();
            gui_move_btn(Vector3.up, "up");
            if (utility.GUI.������("��������", 79))
                manager.������������.��������();
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            gui_move_btn(Vector3.left, "left");
            gui_move_btn(Vector3.back, "-");
            gui_move_btn(Vector3.forward, "+");
            gui_move_btn(Vector3.right, "right");
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            GUILayout.Space(79);
            gui_move_btn(Vector3.down, "down");
            GUILayout.Space(79);
            GUILayout.EndHorizontal();

            GUILayout.EndVertical();

        }

        void Start()
        {          
        }
    }
}
