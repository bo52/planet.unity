﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using utility;
using System.Linq;
public class Chunk
{
    public static byte SCALE = 3;
    public static byte SIZE = (byte)Mathf.Pow(2, SCALE);
    public byte шаг = 0;

    #region ФУНКЦИИ

    static public readonly float[] СмещениеЦентра = new float[]
    {
        0,//0 = 1
        0.5f,//1 = 2
        1.5f,//2 = 4
        3.5f,//3 = 8
        7.5f,//4 = 16
        15.5f,//5 = 32
        31.5f,//6 = 64
        63.5f,//7 = 128
        127.5f,//8 = 512
    };
    static public readonly float[] СмещениеВниз = new float[]
{
        0,//0 = 1
        0.5f,//1 = 2
        0.25f,//2 = 4
        2.5f,//3 = 8
        4.5f,//4 = 16
        15.5f,//5 = 32
        31.5f,//6 = 64
        63.5f,//7 = 128
        127.5f,//8 = 512
};
    static public readonly float[] СмещениеРебра = new float[]
    {
        0,//0=1
        0.5f,//1=2
        0.75f,//2=4
        0.875f,//3=8
        0.9375f,//4=16
        0.96875f,//5=32
        0.984375f,//6=64
        0.9921875f,//7=128
        0.99609375f,//8=512
    };
    static public readonly float[] УменьшитьРебро = new float[]
{
        0,//0=1
        0.5f,//1=2
        0.25f,//2=4
        0.125f,//3=8
        0.0625f,//4=16
        0.03125f,//5=32
        0.015625f,//6=64
        0.0078125f,//7=128
        0.00390625f,//8=512
};

    static public bool ЗаЯчейкой(Vector3 v0)
    {
        if (v0.x < 0) return true;
        if (v0.y < 0) return true;
        if (v0.z < 0) return true;
        if (v0.x >= SIZE) return true;
        if (v0.y >= SIZE) return true;
        if (v0.z >= SIZE) return true;
        return false;
    }
    static public bool заЯчейкой(Vector3 v0)
    {
        if (v0.x < 0) return true;
        if (v0.y < 0) return true;
        if (v0.z < 0) return true;
        if (v0.x > SIZE) return true;
        if (v0.y > SIZE) return true;
        if (v0.z > SIZE) return true;
        return false;
    }

    public bool ЕстьТочка(uint v)
    {
        return cells.ContainsKey(v);
    }

    public bool ЕстьТочка(Vector3 v)
    {
        return cells.ContainsKey(Координата(v));
    }
    #endregion

    public byte ШАГ
    {
        get
        {
            return (byte)Mathf.Pow(2, шаг);
        }
    }
    public byte PART
    {
        get
        {
            return (byte)Mathf.Pow(2, -шаг);
        }
    }

    public static bool Редактор = true;
    public Dictionary<uint, Vector3[]> cells = new Dictionary<uint, Vector3[]>();

    static float АСлучайно
    {
        get
        {
            return 0.25f * Random.Range(0.0f, +1.0f);
        }
    }
    static float Случайно
    {
        get
        {
            return 0.25f * Random.Range(-1.0f, +1.0f);
        }
    }

    static public Vector3[] new_offset
    {
        get
        {
            var offset = new Vector3[6];
            offset[0] = new Vector3(Случайно, Случайно, Случайно);
            offset[1] = new Vector3(-АСлучайно, Случайно, Случайно);
            offset[2] = new Vector3(Случайно, +АСлучайно, Случайно);
            offset[3] = new Vector3(Случайно, -АСлучайно, Случайно);
            offset[4] = new Vector3(Случайно, Случайно, +АСлучайно);
            offset[5] = new Vector3(Случайно, Случайно, -АСлучайно);
            return offset;
        }
    }

    static public uint Координата(Vector3 v)
    {
        return v.get_id_max_min(SIZE, (byte)(SIZE + SIZE));
    }
    static public Vector3 Координата(uint id)
    {
        return id.get_v_max_min(SIZE, (byte)(SIZE + SIZE));
    }

    public Chunk()
    {
        НулеваяПоверхность();
    }

    public void Собрать()
    {
        var v = Vector3.zero;
        this.Собрать(v);      
    }
    public void Сбросить()
    {
        НулеваяПоверхность();
        Собрать();
        simple.manager.ТестоваяЯчейка.НулевоеПоложение();
    }


    public void ИзменитьРазмерЧанка()
    {
        var b = false;
        if (utility.GUI.ползунок(ref шаг, 0, SCALE)) b = true;
        if (utility.GUI.логический(ref Редактор, "Редактор"))
            b = true;

        if (!b) return;
        Собрать();
    }
    
    public void НулеваяПоверхность()
    {
        byte y = 0;
        cells.Clear();
        for (byte x = 0; x <= SIZE; x++)
            //for (byte y = 0; y < cells.GetLength(1); y++)
                for (byte z = 0; z <= SIZE; z++)
                    cells.Add(Координата(new Vector3(x,y,z)), Chunk.new_offset);        
    }
    static public void ВыделитьЧанкНаСцене()
    {
        if (Event.current.keyCode != KeyCode.N) return;

        RaycastHit HIT;
        Ray ray = HandleUtility.GUIPointToWorldRay(Event.current.mousePosition);
        int layerMask = 1 << 10;
        if (!Physics.Raycast(ray, out HIT, Mathf.Infinity, layerMask)) return;
        if (HIT.collider == null) return;       

        var v = HIT.point.ОкруглитьВектор();
        simple.manager.ТестоваяЯчейка.ИзменитьПоложение(v);
    }
}
