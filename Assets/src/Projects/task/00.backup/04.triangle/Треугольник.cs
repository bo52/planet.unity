using UnityEngine;
using UnityEditor;
using utility;
using System.Linq;
using utility.go;
namespace task.triangle
{
    [CustomEditor(typeof(��������))]
    public class gui_�������� : Empty.gui
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            //math.test();
            �������� script = (��������)target;
            script.gui_��������������();
            script.gui_�����������������();
        }
    }


    public class �����������
    {
        public bool ����� = false;
        public Vector3[] vs = new Vector3[3];
        public bool ��������=false;

        static public Vector3 ������������(Vector3 v0, Vector3 v1, Vector3 v2)
        {
            return 0.3333f * new Vector3(v0.x + v1.x + v2.x, v0.y + v1.y + v2.y, v0.z + v1.z + v2.z);
        }
        static public Vector3 �������������(Vector3 v0, Vector3 v1, Vector3 v2)
        {
            return ������������(v0, v1, v2) + Vector3.up;
        }

        static public Vector3 �������(Vector3 v, Vector3 A, Vector3 B)
        {

            Vector3 Side1 = new Vector3(A.x - v.x, A.y - v.y, A.z - v.z);
            Vector3 Side2 = new Vector3(B.x - A.x, B.y - A.y, B.z - A.z);
            Vector3 Perp = Vector3.Cross(Side1, Side2);
            float perpLength = Perp.magnitude;
            return Perp;
        }

        public Vector3 ������������
        {
            get
            {
                return ������������(vs[0], vs[1], vs[2]);
            }
        }
        public Vector3 �������
        {
            get
            {
                return �������(������������, ��������?vs[2]: vs[1], �������� ? vs[1] : vs[2]);
            }
        }
        public Vector3 �������������
        {
            get
            {
                return �������;
            }
        }


        public �����������()
        {
            vs[0] = -Vector3.right - Vector3.up - Vector3.forward;
            vs[1] =  Vector3.right - Vector3.up - Vector3.forward;
            vs[2] =  Vector3.zero - Vector3.up + Vector3.forward;
        }
        public �����������(Vector3 v0, Vector3 v1, Vector3 v2,bool ��������=false)
        {
            vs[0] = v0;
            vs[1] = v1;
            vs[2] = v2;
            this.�������� = ��������;
        }

        public Mesh Triangle(float r = 1)
        {
            return Triangle(vs[0], vs[1], vs[2], ��������, r);
        }

        static public Mesh Triangle(Vector3 v0, Vector3 v1, Vector3 v2, bool ��������, float r = 1)
        {
            return ��������? Triangle(v0, v1, v2, r):Triangle(v2, v1, v0, r);
        }

        static public Mesh Triangle(Vector3 v0, Vector3 v1, Vector3 v2, float r=1)
        {
            var mesh = new Mesh();
            mesh.vertices = new[] { v0, v1, v2 };
            mesh.triangles = new[] { 0, 1, 2 };
            return mesh;
        }
        static public void gui_�����������������()
        {
            if (!utility.GUI.������("�����������������")) return;

            manager.world.transform.����������������();
            GameObject go = new GameObject();
            go.transform.parent = manager.world.transform;
            go.name = "triangle";

            �������� script = go.AddComponent<��������>();
            script.m[0] = new �����������();
            
            script.m[0].Triangle().��������������������(go);
            //Cube(Vector3.right, Vector3.forward, Vector3.up).��������������������(manager.world.transform);
        }

    }

    class �������� : MonoBehaviour
    {
        public �����������[] m = new �����������[4];
        public bool[] �������� = new bool[4];

        public ��������()
        {
        }

        public ��������(����������� m0)
        {
            m[0] = m0;
        }

        public Mesh �������(bool b)
        {
            return �������(m[0].�������������, b);
        }

        public Mesh �������(Vector3 v, bool b)
        {
            //m[0]
            m[1] = new �����������(m[0].vs[1], v, m[0].vs[2],b);
            m[2] = new �����������(m[0].vs[0], m[0].vs[2],v, b);
            m[3] = new �����������(m[0].vs[0], v,m[0].vs[1], b);
            return ���������(b);
        }

        public Mesh ���������(bool b)
        {
            var combine = new CombineInstance[4];
            combine[0].mesh = m[0].Triangle();
            combine[1].mesh = m[1].Triangle();
            combine[2].mesh = m[2].Triangle();
            combine[3].mesh = m[3].Triangle();

            var mesh = new Mesh();
            mesh.CombineMeshes(combine, true, false);
            return mesh;
        }

        public void gui_��������������()
        {
            if (!utility.GUI.������("��������������")) return;

            �������� script = gameObject.AddComponent<��������>();
            gameObject.name = "tetrahedron";
            
            �������(false).��������������������(gameObject);
            m[0].����� = true;
        }
        public void gui_�����������������()
        {
            for (byte i=0;i<4;i++) {
                if (m[i] == null) continue;
                if (m[i].�����) continue;
                if (!utility.GUI.������("�����������������=" + i)) continue;

                GameObject go = new GameObject();
                go.transform.parent = manager.world.transform;
                go.name = "tetrahedron";

                �������� script = go.AddComponent<��������>();

                script.m[0] = m[i];
                script.m[0].�������� = !m[0].��������;
                script.m[0].����� = true;

                script.�������(m[0].��������).��������������������(go);
            }
        }

    }


}
