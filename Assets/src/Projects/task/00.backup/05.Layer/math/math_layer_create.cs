﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace task.Layer
{
    static public class math_layer_create
    {
        static public GameObject layer_create(this GameObject go)
        {
            {
                if (go)
                    return go;

                go = GameObject.FindGameObjectWithTag("editor");
                if (go == null)
                {
                    go = manager.материал("editor");
                    go.name = "editor_layer";
                    go.tag = "editor";
                    go.transform.parent = GameObject.Find("world").transform;
                    go.AddComponent<world>();
                }
                go.transform.localScale = Vector3.one;
                return go;
            }
        }

        static public void gui_СоздатьНовыйСлой()
        {
            if (!utility.GUI.кнопка("СоздатьНовыйСлой")) return;
            GameObject gL=world.editor_layer;
        }
    }
}
