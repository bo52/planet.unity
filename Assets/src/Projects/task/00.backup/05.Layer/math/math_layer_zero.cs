﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using utility.go;
namespace task.Layer
{
    using utility;
    static public class math_layer_zero
    {

        static public void gui_СлойСбросить(this world w)
        {
            if (!utility.GUI.кнопка("СлойСбросить")) return;
            //utility.ulong_id.test();
            math_cube.cube(0).ПривязатьМешКОбъектуОчищенно(w.transform);
        }

    }
}
