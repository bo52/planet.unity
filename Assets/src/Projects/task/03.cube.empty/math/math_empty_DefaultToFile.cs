﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
namespace task.Region.cube.empty
{
    static public class math_empty_defaultToFile
    {
        static public void СформироватьОснову(this StreamWriter sw)
        {
            sw.WriteLine("new List<string>{");
            for (byte x = 1; x < world.SCALE - 1; x++)
                for (byte z = 1; z < world.SCALE - 1; x++)
                    sw.WriteLine(world.КоординатаАтома(new Vector3(x, 0, z))+",");
            sw.WriteLine("},");
        }
        static public void СформироватьУглы(this StreamWriter sw)
        {
            int cnt = -1;
            string h = "";
            string e = "";
            foreach (Vector3 v in new Vector3[] { Vector3.left, Vector3.right, Vector3.back, Vector3.forward })
            {
                cnt++;
                int i = cnt % 2 == 0 ? 0 : world.SCALE - 1;
                foreach (byte T in cnt.СформироватьИндексыУглов())
                    foreach (int j in new int[] { 0, world.SCALE - 1 })
                    {

                        float x = cnt < 2 ? i : j;
                        float z = cnt < 2 ? j : i;
                        e += world.КоординатаАтома(new Vector3(x, 0, z)) + ",";

                        //СформироватьУгловыеВысотыСоседей
                        for (int y = 1; y < world.SCALE; y++)
                            h += world.КоординатаАтома(new Vector3(x, y, z)) + ",";
                    }
            }
            sw.WriteLine("new List<string>{" + e + "},");
            sw.WriteLine("new List<string>{" + h + "},");
        }
        static public void СформироватьЛинииСоседей(this StreamWriter sw)
        {
            int cnt = -1;
            int i = 0;
            sw.WriteLine("{");
            foreach (Vector3 v in new Vector3[] { Vector3.left, Vector3.right, Vector3.back, Vector3.forward })
            {
                cnt++;
                sw.WriteLine("new List<string>{");
                for (int j = 1; j < world.SCALE - 2; j++)
                {
                    i = cnt % 2 == 0 ? 0 : world.SCALE - 1;
                    sw.WriteLine(world.КоординатаАтома(new Vector3(cnt < 2 ? i : j, 0, cnt < 2 ? j : i))+", ");
                }
                sw.WriteLine("},");
            }
            sw.WriteLine("}");
        }
        static public void СформироватьГраниСоседей(this StreamWriter sw)
        {
            int cnt = -1;
            foreach (Vector3 v in new Vector3[] { Vector3.left, Vector3.right, Vector3.back, Vector3.forward })
            {
                cnt++;
                float n = cnt % 2 == 0 ? 0 : world.SCALE - 1;
                for (int i = 1; i < world.SCALE - 1; i++)
                    for (int j = 1; j < world.SCALE - 1; j++)
                    {
                        Vector3 A = cnt < 2 ? new Vector3(n, i, j) : new Vector3(i, j, n);
                        uint a = world.КоординатаАтома(A);
                        sw.WriteLine(a+",");
                    }
            }
        }

        static public void СохранитьВФайл()
        {
            StreamWriter sw = new StreamWriter(Application.dataPath + "/test.ncs", false);
            bool[] bs = new bool[5];
            foreach (bool zero in new bool[] { true, false })
                foreach (bool left in new bool[] { true, false })
                    foreach (bool right in new bool[] { true, false })
                        foreach (bool back in new bool[] { true, false })
                            foreach (bool forward in new bool[] { true, false })
                            {
                                bs[4] = zero;

                                bs[0] = left;
                                bs[1] = right;
                                bs[2] = back;
                                bs[3] = forward;
                            }

            Vector3[] vs = new Vector3[] { Vector3.zero, Vector3.left, Vector3.right, Vector3.back, Vector3.forward };
            foreach (Vector3 v in vs)
            {

            }
            sw.Close();
        }
    }
}
