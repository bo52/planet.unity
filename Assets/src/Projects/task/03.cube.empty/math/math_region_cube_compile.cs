﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using utility;
namespace task.Region.cube.empty
{
    static public class math_region_cube_compile
    {
        static public bool ЭтоТерриторияТриангуляции(this Vector3 dv,block.world R)
        {
            dv = block.math.D*R.Координата+dv;
            float k = 5;
            Vector3 v = manager.player.transform.position;
            if (dv.x > v.x + k) return false;
            if (dv.y > v.y + k) return false;
            if (dv.z > v.z + k) return false;
            if (dv.x < v.x - k) return false;
            if (dv.y < v.y - k) return false;
            if (dv.z < v.z - k) return false;
            return true;
        }

        static public Vector3 НулевойВектор(this Vector3 dv, sbyte scale = -2)
        {
            float part = Mathf.Pow(2, scale);
            dv += 0.5f * Vector3.up;
            dv += 0.5f * part * Vector3.up;
            dv += 1.5f * part * Vector3.left;
            dv += 1.5f * part * Vector3.back;
            return dv;
        }

        static public void ЗакрытьЧистыйТреугольник(this simple.меш m)
        {
            for (byte i = 0; i < 3; i++)
            {
                m.ts.Add(m.vs.Count + i );
            }
        }

        static public void СобираемТреугольник(this simple.меш m, byte i,Vector3 dv, Vector3 v2,Vector3 v3)
        {
            m.ЗакрытьЧистыйТреугольник();
            float part=Mathf.Pow(2, -2);
            m.ДобавитьВершину(4, v2);
            m.ДобавитьВершину(4, dv);

            v3 = i == 3 ? v3 : dv + part * Vector3.right;
            m.ДобавитьВершину(4, dv+ part * Vector3.right);
        }

        static public void НачалоТриангуляции(this simple.меш m,Vector3 dv, sbyte scale = -2)
        {
            //
            //
            //
            //+//+//+//+
            Vector3 v = dv.НулевойВектор();
            Vector3 v2 = dv + 0.5f * меш.ВершиныКуба[2];
            Vector3 v3 = dv + 0.5f * меш.ВершиныКуба[3];
            Vector3 Центр;
            for (byte x = 0; x < 4; x++)
                for (byte z = 0; z < 4; z++)
                {
                    Центр = v + Mathf.Pow(2, scale) * new Vector3(x, 0, z);
                    m.СобираемКвадрат(Центр, 3, scale);
                    if (z==0)
                        m.СобираемТреугольник(x, Центр, v2, v3);
                }
        }

        static public bool ЭтоВерхняяПоверхность(this simple.меш m,block.world R,byte F,Vector3 dv)
        {
            //sbyte scale = -2;
            if (F != 3) return false;
            if (!dv.ЭтоТерриторияТриангуляции(R)) return false;

            m.НачалоТриангуляции(dv);
            //m.СобираемКвадрат(dv, 4, scale);
            return true;
        }

        static public void Кубик(this simple.меш m,block.world R, uint id, Vector3 dv, List<byte> fs)
        {
            foreach (byte f in fs)
            {
                if (m.ЭтоВерхняяПоверхность(R,f, dv)) continue;
                m.СобираемКвадрат(dv, f,0);
            }
        }

    }
}
