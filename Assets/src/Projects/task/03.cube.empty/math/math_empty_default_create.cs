﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Linq;
using utility.go;
namespace task.Region.cube.empty
{
    using utility;
    static public class math_empty_default_create
    {
        static public List<byte> СформироватьИндексыУглов(this int cnt)
        {
            switch (cnt)
            {
                case 0:
                    return new List<byte> { 0, 2 };
                case 1:
                    return new List<byte> { 1, 3 };
                case 2:
                    return new List<byte> { 0, 1 };
                case 3:
                    return new List<byte> { 2, 3 };
                default:
                    return new List<byte>();
            }
        }
        static public List<ulong> СформироватьУглы()
        {
            List<ulong> As = new List<ulong>();
            foreach (int x in new int[] { 0, world.SCALE - 1 })
                foreach (int y in new int[] { 0, world.SCALE - 1 })
                {
                    As.Add(world.КоординатаАтома(new Vector3(x, 0, y)));
                }
            return As;
        }

        static public void СформироватьУглы(this List<Dictionary<uint, ulong>> As, ref List<byte> Углы, int cnt, int I, ulong offset, bool четный)
        {
            List<byte> IS = cnt.СформироватьИндексыУглов();
            int i = четный ? 0 : world.SCALE - 1;
            foreach (byte T in IS)
                foreach (int j in new int[] { 0, world.SCALE - 1 })
                {
                    if (Углы.FindIndex(t => t == T) != -1) continue;
                    Углы.Add(T);
                    
                    float x = cnt < 2 ? i : j;
                    float z = cnt < 2 ? j : i;
                    if (I != 0)
                        As[I].Add(world.КоординатаАтома(new Vector3(x, 0, z)), offset);

                    As.СформироватьУгловыеВысотыСоседей(I, offset, x, z);
                }
        }

        static public void СформироватьУгловыеВысотыСоседей(this List<Dictionary<uint, ulong>> As, int I, ulong offset, float x0, float z0)
        {
            for (int y = 1; y < world.SCALE; y++)
            {
                As[I].Add(world.КоординатаАтома(new Vector3(x0, y, z0)), offset);
            }
        }

        static public List<ulong> СформироватьЛинииСоседей()
        {
            List<ulong> line = new List<ulong>();
            foreach (int z in new int[] { 0, world.SCALE - 1 })
                for (int x = 1; x < world.SCALE - 2; x++)
                    line.Add(world.КоординатаАтома(new Vector3(x, 0, z)));

            foreach (int x in new int[] { 0, world.SCALE - 1 })
                for (int z = 1; z < world.SCALE - 2; z++)
                    line.Add(world.КоординатаАтома(new Vector3(x, 0, z)));

            return line;
        }
        static public void СформироватьЛинииСоседей(this List<Dictionary<uint, ulong>> As,int cnt, int I,ulong offset,bool четный)
        {
            int i = 0;
            for (int j = 1; j < world.SCALE - 2; j++)
            {
                i = четный ? 0 : world.SCALE - 1;
                As[I].Add(
                    world.КоординатаАтома(new Vector3(cnt < 2 ? i : j, 0, cnt < 2 ? j : i)), offset);
            }
        }

        static public void СформироватьГраниСоседей(this List<Dictionary<uint, ulong>> As, int cnt, int I, ulong offset, bool четный)
        {
            float n = четный ? 0 : world.SCALE - 1;
            for (int i = 1; i < world.SCALE - 1; i++)
                for (int j = 1; j < world.SCALE - 1; j++)
                {
                    Vector3 A= cnt < 2 ? new Vector3(n, i, j) : new Vector3(i, j, n);
                    uint a = world.КоординатаАтома(A);
                    As[I].Add(a, offset);
                }
        }

        static public void СформироватьСоседей(this List<Dictionary<uint, ulong>> As , GameObject gw,Vector3 v0,int I,ulong offset)
        {
            int cnt = -1;
            bool четный;
            List<byte> Углы = new List<byte>();

            foreach (Vector3 v in new Vector3[] {Vector3.left,Vector3.right,Vector3.back,Vector3.forward })
            {
                cnt++;
                if (gw.w_REGION().блок.ЕстьКубик(v0, v) == -1) continue;

                четный = cnt % 2 == 0;
                if (I!=0)
                    As.СформироватьЛинииСоседей(cnt, I, offset, четный);                   
                As.СформироватьУглы(ref Углы, cnt, I, offset, четный);
                As.СформироватьГраниСоседей(cnt, I, offset, четный);
            }
        }

        static public List<Dictionary<uint, ulong>> Основа(this GameObject gw0,Vector3 v0)
        {
            ulong offset = world.КоординатаЦентраАтома(Vector3.zero);
            List<Dictionary<uint, ulong>> As = new List<Dictionary<uint, ulong>> {
                new Dictionary<uint, ulong>()
            };

            for (byte x = 1; x < world.SCALE - 1; x++)
                for (byte z = 1; z < world.SCALE - 1; x++)
                    As[0].Add(world.КоординатаАтома(new Vector3(x, 0, z)), offset);

            foreach (uint s in СформироватьУглы())
                As[0].Add(s, offset);
            foreach (uint s in СформироватьЛинииСоседей())
                As[0].Add(s, offset);
            
            //ДвижениеПоВысоте
            Vector3 v = v0;
            GameObject gw = gw0;

            int cnt = 0;
            As.СформироватьСоседей(gw, v, cnt, offset);
            while (Vector3.up.ЕстьКубик(ref gw, ref v) == -1)
            {
                cnt++;
                As.СформироватьСоседей(gw, v, cnt, offset);
            }
            return As;
        }
    }
}
