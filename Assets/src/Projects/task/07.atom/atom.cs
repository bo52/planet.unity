﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using utility;
public class Atom
{
    public bool Активен = true;
    public byte Индекс;
    public string ИД;
    public Atom[] ms = new Atom[8];
    public Atom()
    {
        ИД = funs.Уникальный;
    }
    public Atom(Atom A, byte i)
    {
        this.A = A;
        this.Индекс = i;
        ИД = funs.Уникальный;

    }

    Atom A;
    sbyte scale = 0;

    sbyte SCALE
    {
        get
        {
            return A == null ? scale : A.SCALE;
        }
    }

    public void Создать()
    {
        ms[0] = new Atom(this, 0);
        ms[1] = new Atom(this, 1);
        ms[2] = new Atom(this, 2);
        ms[3] = new Atom(this, 3);
        ms[4] = new Atom(this, 4);
        ms[5] = new Atom(this, 5);
        ms[6] = new Atom(this, 6);
        ms[7] = new Atom(this, 7);
    }

    #region ЦЕНТР
    Vector3 Центр(Vector3 parent)
    {
        return parent + 0.5f * Mathf.Pow(2, scale) * utility.меш.ВершиныКуба[Индекс];

    }
    Vector3 ЦЕНТР
    {
        get
        {
            return A == null ? Vector3.zero : Центр(A.ЦЕНТР);
        }
    }
    #endregion

    #region Строить 
    public Vector3 Вершина(byte F, Vector3 v0, byte i_quad, sbyte Scale)
    {
        return v0 + Mathf.Pow(2, Scale) * utility.меш.ВершиныКуба[utility.меш.ВершиныГранейКуба[F, i_quad]];
    }
    Mesh СтроитьКвадрат(byte F, Vector3 Центр, sbyte Scale)
    {
        var vs = new[] { Вершина(F, Центр, 0, Scale), Вершина(F, Центр, 1, Scale), Вершина(F, Центр, 2, Scale), Вершина(F, Центр, 3, Scale) };

        var mesh = new Mesh
        {
            vertices = vs,
            uv = new[] { F.Вёрстка(vs[0]), F.Вёрстка(vs[1]), F.Вёрстка(vs[2]), F.Вёрстка(vs[3]) },
            triangles = new[] { 0, 2, 3, 3, 1, 0 },
        };
        return mesh;
    }

    void СтроитьКуб(ref List<Mesh> MS, Vector3 parent, sbyte Scale)
    {
        Vector3 v0 = Центр(parent);
        foreach (byte f in new[] { 0, 1, 2, 3, 4, 5 })
        {
            MS.Add(СтроитьКвадрат(f, v0, Scale));
        }
    }

    void Строить(ref List<Mesh> MS, Vector3 parent, sbyte Scale)
    {
        СТРОИТЬ(ref MS, Центр(parent), Scale);
    }

    void СТРОИТЬ(ref List<Mesh> MS, Vector3 v0, sbyte Scale)
    {
        if (Scale < task.Atoms.world.Деталь)
            return;

        foreach (Atom a in ms)
        {
            if (a == null) continue;
            if (!a.Активен) continue;
            a.СтроитьКуб(ref MS, v0, (sbyte)(Scale - 1));
        }
    }

    public void Строить(ref List<Mesh> MS,bool Триангуляция, int index)
    {
        if (Триангуляция)
            ПостроитьКубАтома(ref MS, Vector3.zero, index);
        else
            СТРОИТЬ(ref MS, Vector3.zero, scale);
    }

    #region triangulation
    static void СобратьИндекс(int index, ref List<Mesh> MS,Vector3 v)
    {
        //byte[] m = math_cell_triangle.array[index];
        //for (byte i = 0; i < m.Length/3.0f; i++)
        //{
            //MS.Add(math_triangulation.triangle(
               // 0.5f * math_cell.Рёбра[m[3 * i + 0], 1] + 0.5f * math_cell.Рёбра[m[3 * i + 0], 0],
                //0.5f * math_cell.Рёбра[m[3 * i + 1], 1] + 0.5f * math_cell.Рёбра[m[3 * i + 1], 0],
                //0.5f * math_cell.Рёбра[m[3 * i + 2], 1] + 0.5f * math_cell.Рёбра[m[3 * i + 2], 0], v));
        //}
    }

    public void ПостроитьКубАтома(ref List<Mesh> MS,Vector3 v0, int index)
    {
        if (index == -1)
        foreach (Atom a in ms)
        {
            if (a == null) continue;
            if (!a.Активен) continue;
            index += (int)Mathf.Pow(2, a.Индекс);
        }
        if (index == 0) return;
       СобратьИндекс(index, ref MS,v0);
    }
    #endregion

    #endregion
    public void ОбновитьВсеВершины(int Индекс)
    {
        float id = Индекс;

        for (int n = 7; n >= 0; n--)
        {
            var x = (byte)(id / Mathf.Pow(2, n));
            ms[n].Активен = x != 0;
            id -= Mathf.Pow(2, n) * x;
        }
    }
}
