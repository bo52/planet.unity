﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace task.Region
{
    using Player.hiding;
    static public class math_region_cube_create
    {
        static public void НовыйКубик(this world R, Vector3 dv, uint id, block.ground.меш m, byte fs)
        {
            if (!controller.world.ТолькоПоверхность)
            {
                if (R.блок.СкрытыйБлок(dv)) return;
                R.блок.ПоказатьПоверхность(m, dv);
            }
            m.Кубик(R.блок, id, dv, fs);
        }
    }
}
