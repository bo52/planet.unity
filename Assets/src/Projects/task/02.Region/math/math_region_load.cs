﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
namespace task.Region
{
    using block;
    static public class math_region_load
    {
        static public byte ЗагрузитьРегион(this Vector3 v)
        {
            //id.СозданиеМатерилаДляРегиона();
            ulong id = world.КоординатаРегиона(v);

            byte key = 0;
            if (world.Регионы.ContainsKey(id))
            {
                key = 1;
                if (world.Регионы[id] == null)
                    key = math_region_create.НовыйРегион(id, v, false);
            }
            else
                key = math_region_create.НовыйРегион(id, v,true);
            if (key == 0) return 0;

            world R = world.Регионы[id].GetComponent<world>();
            R.блок.ИД = id;
            //прорисовка кубиков
            switch (key)
            {
                case 1:
                    R.ОбновитьРегион();
                    break;
                case 2:
                    R.w_REGION.ЗагрузитьРегион();
                    break;
            }
            return key;
        }
        static public void ЗагрузкаРегионов()
        {
            int X, Y, Z;
            Vector3 v0 = Player.moving.world.ТекущийРегион_Вектор;

            for (int z = +world.RenderDistanceTables; z >= -world.RenderDistanceTables; z--)
            {
                Z = (int)v0.z + z;
                if (Mathf.Abs(Z) >= world.R) continue;

                for (int y = +world.RenderDistanceTables; y >= -world.RenderDistanceTables; y--)
                {
                    Y = (int)v0.y + y;
                    if (Mathf.Abs(Y) >= world.R) continue;

                    for (int x = +world.RenderDistanceTables; x >= -world.RenderDistanceTables; x--)
                    {
                        X = (int)v0.x + x;
                        if (Mathf.Abs(X) >= world.R) continue;

                        ЗагрузитьРегион(new Vector3(X, Y, Z));
                    }

                }
            }
        }
        static public block.world ЗагружаемыйРегион(this Vector3 v)
        {
            GameObject go = world.ЕстьРегион(v);

            if (go == null)
            {
                if (math_region_create.НовыйРегион(v, true) == 0)
                    return null;
                block.world w = world.ЕстьРегион(v).GetComponent<block.world>();
                w.w_REGION.ЗагрузитьРегион();
            }

            if (go == null) return null;
            return go.GetComponent<block.world>();
        }
        static public void ЗагрузитьРегион(this world R, string f = "")
        {
            if (f == "") f = R.блок.ФАЙЛ;
            char delimeter = ' ';
            R.блок.КубикиРегиона.Clear();

            Vector3 v;
            uint id;
            byte fs;
            uint ps;

            string[] rows = File.ReadAllLines(f);

            block.ground.меш m = new block.ground.меш();
            for (int i = 0; i < rows.Length; i++)
            {
                string[] row = rows[i].Split(delimeter);
                id = uint.Parse(row[0]);
                v = cube.world.КоординатаКубика(id);
                fs = byte.Parse(row[1]);
                ps = uint.Parse(row[2]);

                R.блок.КубикиРегиона.Add(new uint[] { id, fs, ps, 0 });
                R.НовыйКубик(v, id, m, fs);
            }
            m.ПривязатьМешКОбъекту(R.GetComponent<block.ground.world>());
        }

    }
}
