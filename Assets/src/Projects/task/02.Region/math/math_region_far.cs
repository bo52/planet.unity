﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using utility.go;
namespace task.Region
{
    using utility;
    static public class math_region_far
    {
        static public void УдалитьДальниеРегионы(this Vector3 player)
        {
            //if (!RanOnce) return;
            //RanOnce = true;
            List<ulong> l = new List<ulong>();
            foreach (KeyValuePair<ulong, GameObject> T in world.Регионы)
                l.Add(T.Key);

            float d;
            Vector3 v;
            GameObject go;

            player = manager.player.transform.position;
            for (int i = l.Count - 1; i >= 0; i--)
            {
                go = world.Регионы[l[i]].gameObject;
                v = go.transform.position;
                v += block.math.R1 * Vector3.one;
                d = Vector3.Distance(v, player);
                if (d > world.RenderDistanceTables * block.math.D)
                {
                    l[i].УдалитьРегион();
                }
            }
            l.Clear();
        }
        static public void УдалитьРегион(this ulong key)
        {
            GameObject go = world.Регионы[key];
            //очистка региона
            world.Регионы[key] = null;
            world.Регионы.Remove(key);

            go.transform.УдалениеОбъектов();
            Object.DestroyImmediate(go);
        }
    }
}
