﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
//новые соседи
namespace task.Region
{
    using utility;
    using task.Region.cube.editor.add;
    static public class math_neighbor_new
    {
        static byte[] _fs4 = new byte[] { 0, 1, 4, 5 };
        static Vector3[] _vs4 = new Vector3[] { Vector3.left, Vector3.right, Vector3.back, Vector3.forward };

        static public void ОпределитьНовыхСоседей(this world w,int index, Vector3 dv)
        {
            List<byte> FS = w.РедакторКубика.fs(index).SQA().ToList();
            for (byte i = 0; i < 4; i++)
            {
                if (math_f_add.ЕстьПоверхность(FS, _fs4[i]) != -1) continue;
                w.w_REGION.ДобавитьПоверхностьВозможномуКубику(dv, _vs4[i], _fs4[i].ИнверсияГрани());
            }

        }

    }
}
