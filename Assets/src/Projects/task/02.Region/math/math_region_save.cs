﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
namespace task.Region
{
    static public class math_region_save 
    {
        static public void СохранитьДефолтРегион()
        {
            Vector3 v;
            uint ps;
            StreamWriter sw = new StreamWriter(Application.dataPath + "/Resources/Region/default.ncs", false);
            int y = block.math.R1 - 1;
            for (int x = 0; x < block.math.D; x++)
            {
                for (int z = 0; z < block.math.D; z++)
                {
                    v = new Vector3(x, y, z);
                    ps = (uint)Random.Range(0, 1023);
                    sw.WriteLine(cube.world.КоординатаКубика(v) + " 15 " + ps);
                }
            }
            sw.Close();
        }
        static public bool inDefault(this world w)
        {
            if (w.gameObject.transform.position.y < 0) return false;
            if (w.gameObject.transform.position.y > 0) return false;

            w.ЗагрузитьРегион(w.блок.файл);
            w.w_REGION.Сохранить();
            return true;
        }
        static public void Сохранить(this world w)
        {
            StreamWriter sw = new StreamWriter(w.блок.Файл, false);
            foreach (uint[] x in w.блок.КубикиРегиона)
                sw.WriteLine(string.Join(" ", x));
            sw.Close();
        }
        static public void СохранитьПоследний(this world w)
        {
            if (File.Exists(w.блок.Файл))
            {
                StreamWriter sw = new StreamWriter(w.блок.Файл, true);
                sw.WriteLine(string.Join(" ", w.блок.КубикиРегиона[w.блок.КубикиРегиона.Count - 1]));
                sw.Close();
            }
            else
                w.Сохранить();
        }

    }
}
