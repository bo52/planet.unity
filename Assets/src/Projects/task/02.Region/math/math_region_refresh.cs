﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace task.Region
{
    static public class math_region_refresh
    {
        static public void ОбновитьРегион(this world R)
        {
            uint id;
            block.ground.меш m = new block.ground.меш();
            foreach (uint[] x in R.блок.КубикиРегиона)
            {
                id = x[0];
                R.НовыйКубик(cube.world.КоординатаКубика(id), id, m, (byte)x[1]);
            }
            m.ПривязатьМешКОбъекту(R.GetComponent<block.ground.world>());
        }
    }
}
