﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using utility.go;
namespace task.Region
{
    using utility;
    using task.Region.cube.editor.add;
    static public class math_region_next
    {
        static public bool ВходитВРегион(this Vector3 v)
        {
            if (v.x < 0) return false;
            if (v.y < 0) return false;
            if (v.z < 0) return false;
            if (v.x > block.math.D - 1) return false;
            if (v.y > block.math.D - 1) return false;
            if (v.z > block.math.D - 1) return false;
            return true;
        }
        static public bool СледующийРегион(this ref Vector3 v)
        {
            if (v.x >= 0 &&
                v.y >= 0 &&
                v.z >= 0 &&
                v.x <= block.math.D - 1 &&
                v.y <= block.math.D - 1 &&
                v.z <= block.math.D - 1)
                return false;

            if (v.x > block.math.D - 1)
                v.x = 0;
            else if (v.x < 0)
                v.x = block.math.D - 1;

            if (v.y > block.math.D - 1)
                v.y = 0;
            else if (v.y < 0)
                v.y = block.math.D - 1;

            if (v.z > block.math.D - 1)
                v.z = 0;
            else if (v.z < 0)
                v.z = block.math.D - 1;

            return true;
        }
        static public bool СледующийРегион(this ref Vector3 v, ref GameObject gw,Vector3 dv)
        {
            v += dv;
            if (!СледующийРегион(ref v)) return false;

            Vector3 КоординатаРегиона = gw.w_REGION().блок.Координата + dv;
            gw = math_region_load.ЗагружаемыйРегион(КоординатаРегиона).gameObject;
            return true;
        }

        static public bool РегионИзменился(ref Vector3 dv, ref world w, Vector3 v)
        {
            if (ВходитВРегион(dv))
                return false;
            Vector3 КоординатаРегиона = w.блок.Координата + v;
            w = math_region_load.ЗагружаемыйРегион(КоординатаРегиона).w_REGION;
            СледующийРегион(ref dv);
            return true;
        }
        static public void ДобавитьПоверхностьВозможномуКубику(this world w, Vector3 dv, Vector3 v, byte F)
        {
            dv += v;
            math_region_next.РегионИзменился(ref dv, ref w, v);

            uint ID = cube.world.КоординатаКубика(dv);
            List<int> list = w.w_ДобавлениеКубика.ПроверяемДобавлениеКубикаКакПоверхность(ID, F);
            //кубик НЕ существует и ему нужно добавить поверхность
            if (list[0] == -1)
                return;
            //кубик существует и ему нужно добавить поверхность
            w.w_ДобавлениеКубика.ДобавлениеПоверхности(list[0], F);
        }

    }
}
