﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace task.Region
{
    public class math_region_create : MonoBehaviour
    {
        static public GameObject Region(Vector3 dv)
        {
            return Region(world.КоординатаРегиона(dv), dv);
        }

        static public GameObject Region(ulong id,Vector3 dv)
        {
            bool Новый = !block.world.ФайлЕсть(id);
            if (Новый)
            {
                if (dv.y < 0) return null;
                if (dv.y > 0) return null;
            }

            string name = "" + (int)dv.x + (int)dv.y + (int)dv.z;
            GameObject R = GameObject.Find(name);
            if (R == null)
            {
                R = new GameObject();
                R.name = name;
                R.transform.parent = manager.REGION.transform;
                R.AddComponent<block.ground.world>();
                R.AddComponent<hit.world>();
                R.AddComponent<world>();
                R.AddComponent<editor.world>();
                R.AddComponent<cube.editor.world>();
                R.AddComponent<cube.editor.add.world>();
                R.AddComponent<cube.editor.del.world>();

            }
            R.transform.localPosition = block.math.D * dv;
            return R;
        }

        static public byte НовыйРегион(ulong id, Vector3 v, bool нет)
        {
            GameObject go = Region(id, v);
            //регион пустой
            if (go == null) return 0;

            if (нет)
                world.Регионы.Add(id, go);
            else
                world.Регионы[id] = go;
            return 2;
        }

        static public byte НовыйРегион(Vector3 v,bool нет)
        {
            return НовыйРегион(world.КоординатаРегиона(v), v, нет);
        }
    }
}
