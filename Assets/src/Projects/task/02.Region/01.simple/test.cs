﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace task.Region
{
    using utility;
    static public class test
    {
        static public bool Индексация(this меш m, ref int ts,byte fs,int n)
        {
            if (n == 0)
                return false;

            for (byte f = 0; f < fs; f++)
            {
                for (byte g = 0; g < 6; g++)
                    m.ts[ts + 6 * f + g] -= 4 * n;
            }
            //следующие треугольники
            ts += 6 * fs;
            return true;
        }

        static public byte ИнверсияГрани(this byte f)
        {
            switch (f)
            {
                case 0: return 1;
                case 1: return 0;

                case 2: return 3;
                case 3: return 2;

                case 4: return 5;
                case 5: return 4;

            }
            return 0;
        }

        static public int СобираемКвадрат(this List<byte> fs,byte F)
        {
            for (byte i=0; i<fs.Count;i++)
            {
                if (F < fs[i])
                {
                    fs.Insert(i, F);
                    return i;
                }
            }
            fs.Add(F);
            return fs.Count - 1;
        }
        //vs += 4;
        //ts += 6;
        //n += 4;
        //m.СобираемКвадрат(index, v, ((byte)Соседи[i][0]).ИнверсияГрани());
        static public void ГраниСоседа(this меш m, ref List<uint[]> Соседи,uint[] x,bool добавить)
        {
            int INDEX=-1;
            for (int i= Соседи.Count-1; i>=0; i--)
            {
                if (x[0] != Соседи[i][1])
                    continue;

                byte F=((byte)Соседи[i][0]).ИнверсияГрани();

                List<byte> fs = x[1].SQA().ToList();

                INDEX = fs.FindIndex(f => f == F);

                if (добавить)
                {
                    if (INDEX != -1) fs.RemoveAt(INDEX);
                } else
                {
                    if (INDEX == -1) fs.Add(F);
                }


                x[1] = fs.ToArray().Координата(6);
                Соседи.RemoveAt(i);
                return;
            }
        }

        //m.vs.RemoveRange(vs, 4 * fs);
        //m.uvs.RemoveRange(vs, 4 * fs);
        //m.ts.RemoveRange(ts, 6 * fs);
    }
}
