using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using utility;
using utility.go;
namespace task.Region.block.ground
{
    public class ��� : block.���
    {
        simple.��� ground;
        static readonly Vector3[] ����������� = new Vector3[] { Vector3.up, Vector3.down };
        static readonly byte[] fs_y = new byte[] { 3, 2 };
        static byte[] fs = new byte[] { 4, 1,5,0 };
        static Vector3[] vs_cam = new Vector3[] { Vector3.back, Vector3.right, Vector3.forward, Vector3.left };

        public ���()
        {
            ground = new simple.���();
        }

        public bool �������(Vector3 v,Vector3 dv)
        {
            float c = Player.hiding.world.��������������;
            switch (task.Player.rotation.world.pov)
            {               
                case 0: return v.z - c == dv.z;
                case 1: return v.x + c == dv.x;
                case 2: return v.z + c == dv.z;
                case 3: return v.x - c == dv.x;
                default:return false;
            }
        }

        public bool ����������(world R,Vector3 v,sbyte pov,sbyte scale)
        {
            uint �� = cube.world.����������������(v);
            int n = R.�������������.FindIndex(x => x[0] == ��);
            if (n != -1)
                return true; ;
            ground.���������������(v, fs[pov], scale);
            return false;
        }

        public override void �����(block.world _R, uint id, Vector3 dv, byte ifs, sbyte scale = 0)
        {
            if (ifs == 0) return;
            List<byte>FS= ifs.SQA().ToList();

            cube.empty.math_region_cube_compile.�����(this,_R, id, dv, FS);
            world R=(world)_R;
            //���������� ����������� 
            if (!�������(Player.hiding.world.�������������������������, _R.����������������������(dv))) return;
            Vector3 v;

            for (int i=0;i<2;i++)
            {
                v = dv;
                if (FS.FindIndex(x=>x==fs_y[i]) != -1) continue;
                while (v.�������������())
                {
                    v += �����������[i];
                    if (����������(R, v, Player.rotation.world.pov, scale))
                        break;
                }            
            }         
        }
        public override GameObject ��������������������(block.world R)
        {
            R.transform.����������������();
            if (!controller.world.�����������������)
                objects.��������������������(ground, R.transform);

            return objects.��������������������(this,R.transform);
        }
    }
}
