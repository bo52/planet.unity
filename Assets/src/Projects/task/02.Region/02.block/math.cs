using UnityEngine;
using System.Collections.Generic;
using System.IO;

namespace task.Region.block
{
    using utility;
    static public class math
    {
        static public byte scale = 2;
        static public byte D = (byte)Mathf.Pow(2, scale);
        static public byte R1 = (byte)(0.5f * D);
        static public byte R2 = (byte)Mathf.Pow(2, R1);
        static public byte scale2 = (byte)Mathf.Pow(2, D);
        //-1 ��������
        //+0 �������
        //+1 ������
        static public sbyte �������������(this Vector3 v)
        {
            v -= R1 * Vector3.one;
            float r2 = v.magnitude;
            if (Mathf.Abs(v.x) == R1&& 
                Mathf.Abs(v.y) == R1&& 
                Mathf.Abs(v.z) == R1) return 0;
            if (Mathf.Abs(v.x) < R1 &&
                Mathf.Abs(v.y) < R1 &&
                Mathf.Abs(v.z) < R1) return 1;
            if (v.y == 0) return 2;
            return -1;
        }
        static public bool ����������(this Vector3 v, byte f)
        {
            v += Region.array.�����[f];
            if (v.x < 0) return true;
            if (v.y < 0) return true;
            if (v.z < 0) return true;
            if (v.x > D - 1) return true;
            if (v.y > D - 1) return true;
            if (v.z > D - 1) return true;
            return false;
            //return (v + Region.array.�����[f]).�������������() == -1;
        }
        static public bool ��������������(this Vector3 v)
        {
            if (!v.�������������()) return false;
            if (v.x == 0) return true;
            if (v.y == 0) return true;
            if (v.z == 0) return true;
            if (v.x == D - 1) return true;
            if (v.y == D - 1) return true;
            if (v.z == D - 1) return true;
            return false;
        }

        static public bool �������������(this Vector3 v)
        {
            if (v.x < 0) return false;
            if (v.y < 0) return false;
            if (v.z < 0) return false;
            if (v.x > D - 1) return false;
            if (v.y > D - 1) return false;
            if (v.z > D - 1) return false;
            return true;
        }
        static public bool ��������(this Vector3 v)
        {
            return v.y == R1 - 1;
        }
       
    }
}
