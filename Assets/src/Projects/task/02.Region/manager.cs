using UnityEngine;

namespace task.Region
{
    using utility;
    public class manager : simple.manager
    {
        public world WORLD
        {
            get
            {
                return world.GetComponent<Region.world>();
            }
        }
        static GameObject fREGION;
        static public GameObject REGION
        {
            get
            {
                if (fREGION == null)
                {
                    fREGION = GameObject.Find("REGION");
                    if (fREGION == null)
                    {
                        fREGION = new GameObject();
                        fREGION.name = "REGION";
                        fREGION.transform.SetParent(world.transform);
                        fREGION.AddComponent<hit.world>();
                    }
                }

                return fREGION;
            }
        }
 
    }
}
